# target platform defines

ifndef	xwmake/defs/platform.mk
	xwmake/defs/platform.mk = included


PLATFORM_PARAMS  = MEDIUM    MEDIUM_DEV
PLATFORM_PARAMS += $(if $(PART_BOOT_ON),PART_BOOT_ON) PART_BOOT_NUM PART_BOOT_DEV_BLK PART_BOOT_DEV_CHR
PLATFORM_PARAMS += $(if $(PART_PARM_ON),PART_PARM_ON) PART_PARM_NUM PART_PARM_DEV_BLK PART_PARM_DEV_CHR
PLATFORM_PARAMS += $(if $(PART_KERN_ON),PART_KERN_ON) PART_KERN_NUM PART_KERN_DEV_BLK PART_KERN_DEV_CHR
PLATFORM_PARAMS +=                      PART_ROOT_ON  PART_ROOT_NUM PART_ROOT_DEV_BLK PART_ROOT_DEV_CHR PART_ROOT_FS PART_ROOT_RO
PLATFORM_PARAMS += $(if $(PART_CONF_ON),PART_CONF_ON) PART_CONF_NUM PART_CONF_DEV_BLK PART_CONF_DEV_CHR PART_CONF_FS PART_CONF_MNT
PLATFORM_PARAMS += MACADR_DEV_CHR
PLATFORM_PARAMS +=                          CONS_DEV CONS_SPEED


ifeq ($(PLATFORM),build)
 include $(XWMAKE_DIR)/platform/build.mk
else
-include $(PLATFORM_MK)
endif

# normalized arch
# .. lang=ru
# ��������������� :make:var:`ARCH`.
# �������� �� ���������: :make:var:`ARCH`.
NARCH  ?= $(ARCH)

ifeq ($(LIBC_TYPE),g)
	target_libc = gnu
else ifeq ($(LIBC_TYPE),uc)
	target_libc = uclibc
endif

# GNU Target Triplet:
# CPU - vendor|platform|board - OS [- library]
# .. lang=ru
# :term:`GNU Target Triplet <GNU Target Triplet>`
TARGET ?= $(ARCH)-$(PLATFORM)-linux$(if $(target_libc),-$(target_libc))$(ABI)


endif # xwmake/defs/platform.mk
