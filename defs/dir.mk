# directories
ifndef	xwmake/defs/dir.mk
	xwmake/defs/dir.mk = included


# TOP_DIR (supplied from script)
# .. lang=ru
# ������� �������� ������.
# ������������ �������� ������� ��������� xwmake.
TOP_DIR ?= $(error TOP_DIR undefined)

# XWMAKE_DIR (supplied from script)
# .. lang=ru
# ������� ��������� ������� ������.
# ������������ �������� ������� ��������� xwmake.
XWMAKE_DIR ?= $(error XWMAKE_DIR undefined)

# root of a project
# PRJ_DIR (supplied from script)
# .. lang=ru
# ������� �������.
# ������������ �������� ������� ��������� xwmake.
# ����������� :ref:`�������� ��������� ��������� <xwmake-setup>`
# �� ����� :ref:`��������� ��������� <��������� ���������>`.
PRJ_DIR ?= $(error PRJ_DIR undefined)

# common dir for all projects
# .. lang=ru
# ������� ����� ����� ��������.
COMMON_DIR ?= $(TOP_DIR)/common_xw

# .. lang=ru
# ������� ������� ������������
TOOLS_DIR ?= $(TOP_DIR)/tools

# where package source tar files searched - local read only repository
# DEPOT_DIR (supplied from script)
# .. lang=ru
# ������� �������� �������� ������� ������� ������.
# ������������ �������� ������� ��������� xwmake.
# ����������� :ref:`�������� ��������� ��������� <xwmake-setup>`
# �� ����� :ref:`��������� ��������� <��������� ���������>`.
DEPOT_DIR ?= $(error DEPOT_DIR undefined)

# all writes goes here; root of any build/install procedure
#   VAR_DIR (supplied from script)
# .. lang=ru
# ������� ������ �������.
# ������������ �������� ������� ��������� xwmake.
# ����������� :ref:`�������� ��������� ��������� <xwmake-setup>`
# �� ����� :ref:`��������� ��������� <��������� ���������>`.
VAR_DIR ?= $(error VAR_DIR undefined)

# .. lang=ru
# ���������� ��� ��������� ������.
TMP_DIR = $(VAR_DIR)/tmp

# status/stamps dir
# .. lang=ru
# �������� ������� ������ ��������� ������ �������.
 _ST_DIR = $(VAR_DIR)/st

# download dir; new package source tar files downloaded here
# �������� ������� �������� ������� �������� �������
# :ref:`������� ������ <two-level-archive>`
 DL_DIR = $(VAR_DIR)/dl

# base pristine sources dir, where extract takes place
# .. lang=ru
# �������� ������� ��� ���������� �������� ������� �� �������
   ORIG_SRC_DIR = $(VAR_DIR)/src/origin

# linked from pristine source and patched by platform-specific patches
# .. lang=ru
# ���������� ��� ��������� ������ �� �������� ������ ��� ������� ���������.
PATCHED_SRC_DIR = $(VAR_DIR)/src/$(PLATFORM)


# the work base directory, where build takes place
# .. lang=ru
# �������� ������� ������ �������.
_BLD_DIR = $(VAR_DIR)/bld

# .. lang=ru
# �������� ������� �������� ������ �������.
_LOG_DIR = $(VAR_DIR)/log

# installwatch lists dir
# .. lang=ru
# �������� ������� ������� ������, ��������������� ��������
_IWL_DIR = $(VAR_DIR)/iwl

# dir for installation of other things
# .. lang=ru
# �������� ������� ��������� �������.
_INST_DIR = $(VAR_DIR)/inst

# target file system root
# .. lang=ru
# �������� ������� �������� �������� ������� ������� ������
ifeq ($(PLATFORM),build)
_ROOT_DIR = $(_INST_DIR)
else
_ROOT_DIR = $(VAR_DIR)/root
endif

# build machine tools common dir
# .. lang=ru
# �������� ������� ��������� ������������ ��������� ������.
# Deprecated.
_TOOL_DIR = $(error _TOOL_DIR deprecated, use _INST_DIR instead)

# .. lang=ru
# ������� ������ ������� �������.
   PRJ_PKG_DIR =    $(PRJ_DIR)/pkg

# .. lang=ru
# ������� ������ ������� ����� ����� ��������.
COMMON_PKG_DIR = $(COMMON_DIR)/pkg

# .. lang=ru
# ������� ������ ������� ������������
TOOLS_PKG_DIR = $(TOOLS_DIR)/pkg

# .. lang=ru
# ������� ������ ������� ��������� ������� ������.
XWMAKE_PKG_DIR = $(XWMAKE_DIR)/pkg

# .. lang=ru
# ������� ������ ������� ���������.
PLATFORM_DIR = $(PRJ_DIR)/platform/$(PLATFORM)

# .. lang=ru
# ���� �������� ������� ���������.
PLATFORM_MK  = $(PRJ_DIR)/platform/$(PLATFORM).mk


# .. lang=ru
# ������� ������ ������� �������.
# ������������ :ref:`�������� ������� ��������� <b-project>`.
# ������������ :ref:`�������� ��������� ��������� <xwmake-setup>`
# �� ����� :ref:`��������� ��������� <��������� ���������>`.

# .. lang=ru
# ���������� ������ ��������� ������ ������� ��� ������� ���������.
  ST_DIR =   $(_ST_DIR)/$(PLATFORM)

# .. lang=ru
# ���������� ������ ��������� ������ ������� ��� ��������� build.
 BST_DIR =   $(_ST_DIR)/build

# .. lang=ru
# ���������� �������� ������ ������� ��� ������� ���������.
 LOG_DIR =  $(_LOG_DIR)/$(PLATFORM)

# .. lang=ru
# ���������� ������� ������, ��������������� �������� ��� ������� ���������.
 IWL_DIR =  $(_IWL_DIR)/$(PLATFORM)

# .. lang=ru
# ���������� ������ ������� ��� ������� ���������.
 BLD_DIR =  $(_BLD_DIR)/$(PLATFORM)

# .. lang=ru
# ���������� ��������� ������� ��� ������� ���������.
INST_DIR = $(_INST_DIR)/$(PLATFORM)

# .. lang=ru
# ���������� �������� �������� ������� ��� ������� ���������.
ROOT_DIR = $(_ROOT_DIR)/$(PLATFORM)

# .. lang=ru
# ���������� ������������ ��������� ������ ��� ��������� ���������.
BINST_DIR = $(_INST_DIR)/build

# .. lang=ru
# ���������� ������ ��������� ������ ��� ��������� ���������.
BBLD_DIR = $(_BLD_DIR)/build


# compatibility
 TOOL_DIR = $(error TOOL_DIR deprecated, use BINST_DIR instead)


# %fetch update %extract %found
  SRC_ST_DIR = $(_ST_DIR)/src
# %patch
PATCH_ST_DIR = $(_ST_DIR)/$(PLATFORM)


# .. lang=ru
# ������� �������� ������� �������, ����ޣ���� � ������ ���������������
   PRJ_SRC_DIR =    $(PRJ_DIR)/src

# .. lang=ru
# ������� �������� ������� ����� ����� ���� ��������, ����ޣ���� ���������������
COMMON_SRC_DIR = $(COMMON_DIR)/src

# .. lang=ru
# ������� �������� ������� ������������
TOOLS_SRC_DIR = $(TOOLS_DIR)/src


# .. lang=ru
# ���������� ����� ������.
# ����� ���� ��� ����� ��������� �������,
# ��� � ������������ � ������� ������ ������.
PKG_PKG_DIR = $(dir $(firstword $(MAKEFILE_LIST)))


endif # xwmake/defs/dir.mk
