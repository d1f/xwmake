ifndef	xwmake/defs/iw.mk
	xwmake/defs/iw.mk = included


# IWTYPE from environment: installwatch | strace | no
# .. lang=ru
# ��� ��������� ������������ ������ ��������������� ������� ������.
# ��������� ��������: installwatch, strace, no
# �������� �� ���������: installwatch.
IWTYPE ?= installwatch

ifeq      ($(IWTYPE),installwatch)
# .. lang=ru
# ��������� �������� �� ���������������� �������.
# �������������: $(IW) ������� [��������� ...] $(LOG)
  IW = $(BINST_DIR)/bin/installwatch -r $(IWL_DIR) -o $(IWL_DIR)/$(PKG).iwl
  UNINSTALL_SCRIPT = $(XWMAKE_SCRIPT_DIR)/uninstall_installwatch
else ifeq ($(IWTYPE),strace)
  IW = $(XWMAKE_SCRIPT_DIR)/instwatch-strace         $(IWL_DIR)/$(PKG).iwl
  UNINSTALL_SCRIPT = $(XWMAKE_SCRIPT_DIR)/uninstall_strace
else ifeq ($(IWTYPE),no)
  IW =
  UNINSTALL_SCRIPT = true
else
  $(error Unknown IWTYPE: $(IWTYPE))
endif

# .. lang=ru
# ��������� �������� �� ���������������� �������,
# ���� ������������� ������� �������� �������� ��������.
# �������������: $(IWSH) "������� [��������� ...]" $(LOG)
IWSH = $(IW) $(SHELL) -c

ifneq ($(IWTYPE),no)
  define UNINSTALL_CMD
    if test -f             $(IWL_DIR)/$(PKG).iwl; then \
       $(UNINSTALL_SCRIPT) $(IWL_DIR)/$(PKG).iwl ^$(PKG_BLD_DIR) $(LOG); \
    fi
    $(RM) $(IWL_DIR)/$(PKG).iwl
  endef
endif


LIST_INSTALL = \
  list_inst() \
  {\
    local iwl=$${1:-$(IWL_DIR)/$(PKG).iwl}; \
    if test -f $$iwl; then \
       $(UNINSTALL_SCRIPT) --list-only $$iwl ^$(PKG_BLD_DIR); \
    fi; \
  };\
  list_inst

# .. lang=ru
# �������, �������� �� ����������� �����, ������ ������������� �������
# � �������� ��, ������� ������.
LIST_INSTALL_FILES = $(LIST_INSTALL) 2>/dev/null | \
	egrep "^file $(ROOT_DIR)" | cut '-d ' -f2


endif # xwmake/defs/iw.mk
