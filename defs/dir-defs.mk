# common directory defines
ifndef	xwmake/defs/dir-defs.mk
	xwmake/defs/dir-defs.mk = included


  BOOT_DIR ?= /boot
 RBOOT_DIR ?= $(ROOT_DIR)$(BOOT_DIR)

   WEB_DIR ?= /var/www
  RWEB_DIR ?=  $(ROOT_DIR)$(WEB_DIR)

   ETC_DIR ?= /etc_
  RETC_DIR ?=  $(ROOT_DIR)$(ETC_DIR)
  IETC_DIR ?=  $(INST_DIR)$(ETC_DIR)

  CONF_DIR ?= /conf
 RCONF_DIR ?=  $(ROOT_DIR)$(CONF_DIR)
 ICONF_DIR ?=  $(INST_DIR)$(CONF_DIR)

  INIT_DIR ?= /init
 RINIT_DIR ?= $(ROOT_DIR)$(INIT_DIR)

   BIN_DIR ?= /bin
  RBIN_DIR ?=  $(ROOT_DIR)$(BIN_DIR)
  IBIN_DIR ?=  $(INST_DIR)$(BIN_DIR)
  BBIN_DIR ?= $(BINST_DIR)$(BIN_DIR)
  TBIN_DIR  = $(error TBIN_DIR deprecated, use BBIN_DIR instead)


  SBIN_DIR ?= /sbin
 RSBIN_DIR ?=  $(ROOT_DIR)$(SBIN_DIR)
 ISBIN_DIR ?=  $(INST_DIR)$(SBIN_DIR)
 BSBIN_DIR ?= $(BINST_DIR)$(SBIN_DIR)
 TSBIN_DIR  = $(error TSBIN_DIR deprecated, use BSBIN_DIR instead)

   LIB_DIR ?= /lib
  RLIB_DIR ?=  $(ROOT_DIR)$(LIB_DIR)
  ILIB_DIR ?=  $(INST_DIR)$(LIB_DIR)
  BLIB_DIR ?= $(BINST_DIR)$(LIB_DIR)
  TLIB_DIR  = $(error TLIB_DIR deprecated, use BLIB_DIR instead)

 LIBEXEC_DIR ?= /libexec
RLIBEXEC_DIR ?=  $(ROOT_DIR)$(LIBEXEC_DIR)

   INC_DIR ?= /include
  RINC_DIR ?=  $(ROOT_DIR)$(INC_DIR)
  IINC_DIR ?=  $(INST_DIR)$(INC_DIR)
  BINC_DIR ?= $(BINST_DIR)$(INC_DIR)
  TINC_DIR  = $(error TINC_DIR deprecated, use BINC_DIR instead)

 SHARE_DIR ?= /share
RSHARE_DIR ?=  $(ROOT_DIR)$(SHARE_DIR)
ISHARE_DIR ?=  $(INST_DIR)$(SHARE_DIR)
BSHARE_DIR ?= $(BINST_DIR)$(SHARE_DIR)
TSHARE_DIR  = $(error TSHARE_DIR deprecated, use BSHARE_DIR instead)

   MAN_DIR ?= /man
  RMAN_DIR ?=  $(ROOT_DIR)$(MAN_DIR)
  IMAN_DIR ?=  $(INST_DIR)$(MAN_DIR)
  BMAN_DIR ?= $(BINST_DIR)$(MAN_DIR)
  TMAN_DIR  = $(error TMAN_DIR deprecated, use BMAN_DIR instead)

 LKMOD_DIR ?= /lib/modules/$(LK_RELEASE)
RLKMOD_DIR ?=    $(ROOT_DIR)$(LKMOD_DIR)


      IMAGES_DIR ?=   $(INST_DIR)/images
LABEL_IMAGES_DIR ?= $(IMAGES_DIR)/$(LABEL)


endif # xwmake/defs/dir-defs.mk
