# standard targets
ifndef	xwmake/defs/target.mk
	xwmake/defs/target.mk = included


       SRC_TARGETS   = fetch fetch.main fetch.patch check extract
     PATCH_TARGETS   = patch
  PLATFORM_TARGETS   = link pre-configure configure depend build install install-root pack pack-status
       ALL_TARGETS   = $(SRC_TARGETS) $(PATCH_TARGETS) $(PLATFORM_TARGETS)

     SRC_UNTARGETS   = $(addprefix un,$(SRC_TARGETS))
   PATCH_UNTARGETS   = $(addprefix un,$(PATCH_TARGETS))
PLATFORM_UNTARGETS   = $(addprefix un,$(PLATFORM_TARGETS))
     ALL_UNTARGETS   = $(addprefix un,$(ALL_TARGETS))

     SRC_RETARGETS   = $(addprefix re,$(SRC_TARGETS))
   PATCH_RETARGETS   = $(addprefix re,$(PATCH_TARGETS))
PLATFORM_RETARGETS   = $(addprefix re,$(PLATFORM_TARGETS))
     ALL_RETARGETS   = $(addprefix re,$(ALL_TARGETS))

     SRC_ALL_TARGETS =      $(SRC_TARGETS)      $(SRC_UNTARGETS)      $(SRC_RETARGETS)
   PATCH_ALL_TARGETS =    $(PATCH_TARGETS)    $(PATCH_UNTARGETS)    $(PATCH_RETARGETS)
PLATFORM_ALL_TARGETS = $(PLATFORM_TARGETS) $(PLATFORM_UNTARGETS) $(PLATFORM_RETARGETS)

   NOT_BUILD_TARGETS = fetch% check %extract %patch link none all %clean un% %list update save%

.PHONY : $(ALL_TARGETS) $(ALL_UNTARGETS) $(ALL_RETARGETS)

# phony stamps for dependend packages
.PHONY : $(addprefix   $(SRC_ST_DIR)/%.,$(SRC_UNTARGETS))
.PHONY : $(addprefix $(PATCH_ST_DIR)/%.,$(PATCH_UNTARGETS))
.PHONY : $(addprefix       $(ST_DIR)/%.,$(PLATFORM_UNTARGETS))

.DELETE_ON_ERROR : $(addprefix   $(SRC_ST_DIR)/%.,$(SRC_TARGETS))
.DELETE_ON_ERROR : $(addprefix $(PATCH_ST_DIR)/%.,$(PATCH_TARGETS))
.DELETE_ON_ERROR : $(addprefix       $(ST_DIR)/%.,$(PLATFORM_TARGETS))


endif # xwmake/defs/target.mk
