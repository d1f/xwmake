ifndef	xwmake/defs/mirrors.mk
	xwmake/defs/mirrors.mk = included


# PKG_SITES (mirrors) lists
#
# format: URL, including some base path



#+ kernel.org mirrors
#  http://www.kernel.org/mirrors/

# .. lang=ru
# ������ ������ Linux � ������.
LK_ORG_RU += http://mirror.yandex.ru/kernel.org/linux
LK_ORG_RU +=  ftp://ftp.ru.kernel.org/pub/linux

# .. lang=ru
# ������ ������ Linux � �����������.
LK_ORG_NL += http://www.nl.kernel.org/pub/linux

# .. lang=ru
# �������� ���� Linux.
LK_ORG    += http://www.kernel.org/pub/linux

#- kernel.org mirrors


#+ gnu mirrors
#  http://www.gnu.org/order/ftp.html

#+ dead
#GNU_ORG_RU += http://www.mirrors.net.ru/pub/mirrors/gnu/ftp/gnu
#GNU_ORG_RU += ftp://ftp.mirrors.net.ru/pub/mirrors/gnu/ftp/gnu
#GNU_ORG_RU += ftp://ftp.chg.ru/pub/gnu
#- dead
#GNU_ORG_RU += ftp.sibsutis.ru/pub/mirrors/ftp.gnu.org
#GNU_ORG_RU += ftp.netis.ru/pub/software/unix/gnu
#GNU_ORG_RU += ftp.sai.msu.su/pub/GNU.new
#GNU_ORG_RU += postgresql.sai.msu.ru/pub/GNU.new
#GNU_ORG_RU += ftp.kiae.su/pub/gnu/gnu-mirror

# .. lang=ru
# ������ ������ GNU � �����������.
GNU_ORG_NL += ftp://ftp.nluug.nl/pub/gnu
GNU_ORG_NL += ftp://ftp.mirror.nl/pub/mirror/gnu
GNU_ORG_NL += ftp://ftp.nl.uu.net/pub/gnu
GNU_ORG_NL += ftp://mirror.widexs.nl/pub/gnu
GNU_ORG_NL += ftp://ftp.win.tue.nl/pub/gnu
#GNU_ORG_NL += ftp://gnu.mirror.vuurwerk.net/pub/GNU
GNU_ORG_NL += ftp://gnu.kookel.org/pub/ftp.gnu.org

# .. lang=ru
# �������� ���� GNU.
GNU_ORG    += http://ftp.gnu.org/pub/gnu

#- gnu mirrors



#+ debian mirrors

# .. lang=ru
# ������ ������ Debian GNU/Linux � ���������.
DEB_ORG_FI  = http://ftp.fi.debian.org/debian

# .. lang=ru
# ������ ������ Debian GNU/Linux � �����������.
   DEB_ORG_NL  = http://ftp.nl.debian.org/debian
SECDEB_ORG_NL  = http://ftp.nl.debian.org/debian-security
   DEB_ORG_NL += ftp://gnu.mirror.vuurwerk.net/pub/debian

# .. lang=ru
# ������ ������ Debian GNU/Linux � ������.
   DEB_ORG_RU += http://mirror.yandex.ru/debian
SECDEB_ORG_RU += http://mirror.yandex.ru/debian-security
   DEB_ORG_RU += http://ftp.ru.debian.org/debian
SECDEB_ORG_RU += http://ftp.ru.debian.org/debian-security
   DEB_ORG_RU +=  ftp://ftp.ru.debian.org/debian
SECDEB_ORG_RU +=  ftp://ftp.ru.debian.org/debian-security

# .. lang=ru
# ������ ������ Debian GNU/Linux � ������������.
# offline ...
#   DEB_ORG_NSK  = http://debian.nsu.ru/debian
#SECDEB_ORG_NSK += http://debian.nsu.ru/debian-security

# .. lang=ru
# ������ ������ Debian GNU/Linux � ���.
DEB_ORG_US  = http://ftp.us.debian.org/debian

# .. lang=ru
# �������� ���� Debian GNU/Linux.
   DEB_ORG  = http://ftp.debian.org/debian
SECDEB_ORG  = http://security.debian.org/debian-security

   DEB_ARCHIVE  = http://archive.debian.org/debian
SECDEB_ARCHIVE += http://archive.debian.org/debian-security

#- debian mirrors


#+ sourceforge mirrors
#  http://sourceforge.net/apps/trac/sourceforge/wiki/Mirrors

# .. lang=ru
# ������ ���������� ������ SourceForge � ���������.
SFNET_AU  = internode		# Adelaide, Australia (Australia)
SFNET_AU += transact		# Canberra, Australia (Australia)
SFNET_AU += waix		# Perth, Australia (Australia)

# .. lang=ru
# ������ ���������� ������ SourceForge � ��������.
SFNET_BR  = ufpr		# Curitiba, Brazil (South America)

# .. lang=ru
# ������ ���������� ������ SourceForge � ������.
SFNET_CA  = iweb		# Quebec, Canada (North America)

# .. lang=ru
# ������ ���������� ������ SourceForge � ��������.
SFNET_DE  = dfn			# Berlin, Germany (Europe)
SFNET_DE += mesh		# Duesseldorf, Germany (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � �������.
SFNET_FR  = freefr		# France (Europe)
SFNET_FR += ovh			# Paris, France (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ��������.
SFNET_IE += heanet		# Dublin, Ireland (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ���������.
SFNET_IN += biznetnetworks	# Indonesia (Asia)

# .. lang=ru
# ������ ���������� ������ SourceForge � ������.
SFNET_IT  = fastbull		# Italy (Europe)
SFNET_IT += garr		# Bologna, Italy (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ������.
SFNET_JP += jaist		# Ishikawa, Japan (Asia)

# .. lang=ru
# ������ ���������� ������ SourceForge � �����������.
SFNET_NL += surfnet		# Amsterdam, The Netherlands (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ����������.
SFNET_PT += nfsi		# Linda-a-Velha, Portugal (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ������.
SFNET_RU += citylan

# .. lang=ru
# ������ ���������� ������ SourceForge � ������.
SFNET_SE += sunet		# Sweden (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ���������.
SFNET_SW  = puzzle		# Bern, Switzerland (Europe)
SFNET_SW += switch		# Lausanne, Switzerland (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � �������.
SFNET_TW  = nchc		# Tainan, Taiwan (Asia)
SFNET_TW += ncu			# Taiwan (Asia)

# .. lang=ru
# ������ ���������� ������ SourceForge � ��������������.
SFNET_UK  = kent		# Kent, UK (Europe)

# .. lang=ru
# ������ ���������� ������ SourceForge � ���.
SFNET_US  = hivelocity		# Tampa, FL, USA (North America)
SFNET_US += internap		# San Jose, CA, USA (North America)
SFNET_US += softlayer		# Plano, TX (North America)
SFNET_US += superb-east		# McLean, VA, USA (North America)
SFNET_US += superb-west		# McLean, VA, USA (North America)
SFNET_US += voxel		# New York, NY, USA (North America)

# .. lang=ru
# ������ ���������� ������ SourceForge � ������.
SFNET_EUROPE  = $(SFNET_NL) $(SFNET_DE) $(SFNET_SW) $(SFNET_SE)
SFNET_EUROPE += $(SFNET_UK) $(SFNET_IE)
SFNET_EUROPE += $(SFNET_FR) $(SFNET_IT) $(SFNET_PT)

sf_net_mirrors_f = $(addprefix http://,$(addsuffix .dl.sourceforge.net/sourceforge,$(1)))

# .. lang=ru
# ������ ������ SourceForge � ���������.
SF_NET_AU = $(call sf_net_mirrors_f,$(SFNET_AU))

# .. lang=ru
# ������ ������ SourceForge � ���������.
SF_NET_CH = $(call sf_net_mirrors_f,$(SFNET_CH))

# .. lang=ru
# ������ ������ SourceForge � ��������.
SF_NET_DE = $(call sf_net_mirrors_f,$(SFNET_DE))

# .. lang=ru
# ������ ������ SourceForge � �������.
SF_NET_FR = $(call sf_net_mirrors_f,$(SFNET_FR))

# .. lang=ru
# ������ ������ SourceForge � ��������.
SF_NET_IE = $(call sf_net_mirrors_f,$(SFNET_IE))

# .. lang=ru
# ������ ������ SourceForge � ������.
SF_NET_IT = $(call sf_net_mirrors_f,$(SFNET_IT))

# .. lang=ru
# ������ ������ SourceForge � ������.
SF_NET_RU = $(call sf_net_mirrors_f,$(SFNET_RU))

# .. lang=ru
# ������ ������ SourceForge � ��������������.
SF_NET_UK = $(call sf_net_mirrors_f,$(SFNET_UK))

# .. lang=ru
# ������ ������ SourceForge � ���.
SF_NET_US = $(call sf_net_mirrors_f,$(SFNET_US))


# .. lang=ru
# ������ ������ SourceForge � ������.
SF_NET_EUROPE = $(call sf_net_mirrors_f,$(SFNET_EUROPE))

#- sourceforge mirrors


#+ apache mirrors

# .. lang=ru
# ������ ������ Apache � �����������.
APACHE_ORG_NL  = http://apache.mirror.intouch.nl/
APACHE_ORG_NL += http://apache.essentkabel.com/
APACHE_ORG_NL += http://apache.pagefault.net/
APACHE_ORG_NL += http://httpd.kookel.org/
APACHE_ORG_NL += http://apache.cs.uu.nl/dist/

# .. lang=ru
# ������ ������ Apache � ������.
APACHE_ORG_RU  = http://apache.rinet.ru/dist
APACHE_ORG_RU += http://apache.officepark.ru
APACHE_ORG_RU += http://www.sai.msu.su/apache/dist

# .. lang=ru
# ������ ������ Apache � �������.
APACHE_ORG_UA  = http://apache.alkar.net
APACHE_ORG_UA += http://apache.dnepr.net

# .. lang=ru
# ������ ������ Apache � �������.
APACHE_ORG_TW  = http://apache.stu.edu.tw
APACHE_ORG_TW += http://ftp.cis.nctu.edu.tw/data/UNIX/apache

# .. lang=ru
# ������ ������ Apache � ���.
APACHE_ORG_US  = http://www.tux.org/pub/net/apache/dist
APACHE_ORG_US += http://www.ibiblio.org/pub/mirrors/apache
APACHE_ORG_US += http://www.reverse.net/pub/apache

# .. lang=ru
# �������� ���� Apache.
APACHE_ORG  = http://www.eu.apache.org/dist
APACHE_ORG += http://www.apache.org/dist
#- apache mirrors


-include $(_ST_DIR)/hostname.mk


# .. lang=ru
# ������ ������ ������� �������� ������� �������
SRCARCHIVE_SITES  =

# .. lang=ru
# ���� � ���� ������� �������� ������� �������, ����������� ������.
# ���� �� ����� ��������� ���������, ��� ������ � ����� ��������.
# ������������ ��������.
SRCARCHIVE_SITE_FIRST ?=

# .. lang=ru
# ���� � ���� ������� �������� ������� �������, ����������� ���������.
# ���� �� ����� ��������� ���������, ��� ������ � ����� ��������.
# ������������ ��������.
SRCARCHIVE_SITE_LAST  ?=


# .. lang=ru
# ������ ������ Debian GNU/Linux
   DEB_ORG_SITES  = $(SRCARCHIVE_SITES)


ifneq ($(strip $(filter  %.nsk.su destiny live.1nsk.ru %.obnet.lan %.sigrand.lcl %.academ.org %.academ.local,$(HOSTNAME_F))),)
   DEB_ORG_SITES +=    $(DEB_ORG_NSK)
SECDEB_ORG_SITES += $(SECDEB_ORG_NSK)
SRCARCHIVE_SITES += $(SRCARCHIVE_RU)
endif
ifneq ($(strip $(filter %.su %.ru destiny live.1nsk.ru %.obnet.lan %.sigrand.lcl %.academ.org %.academ.local,$(HOSTNAME_F))),)
# .. lang=ru
# ������ ������ Linux
    LK_ORG_SITES +=  $(LK_ORG_RU)
# .. lang=ru
# ������ ������ GNU
   GNU_ORG_SITES +=    $(GNU_ORG_RU)    $(GNU_ORG_NL)
   DEB_ORG_SITES +=    $(DEB_ORG_RU)    $(DEB_ORG_FI)
SECDEB_ORG_SITES += $(SECDEB_ORG_RU) $(SECDEB_ORG_FI)
# .. lang=ru
# ������ ������ SourceForge
    SF_NET_SITES += $(SF_NET_RU) $(SF_NET_EUROPE) $(SF_NET_US)
# .. lang=ru
# ������ ������ Apache
APACHE_ORG_SITES += $(APACHE_ORG_RU)
endif

ifneq ($(strip $(filter %.ua,$(HOSTNAME_F))),)
APACHE_ORG_SITES += $(APACHE_ORG_UA)
endif

ifneq ($(strip $(filter %.nl,$(HOSTNAME_F))),)
    LK_ORG_SITES +=  $(LK_ORG_NL)
   GNU_ORG_SITES += $(GNU_ORG_NL)
   DEB_ORG_SITES +=    $(DEB_ORG_NL)
SECDEB_ORG_SITES += $(SECDEB_ORG_NL)
    SF_NET_SITES += $(SF_NET_EUROPE)
APACHE_ORG_SITES += $(APACHE_ORG_NL)
endif

    LK_ORG_SITES +=  $(LK_ORG)
   GNU_ORG_SITES += $(GNU_ORG)
   DEB_ORG_SITES +=    $(DEB_ORG_US)    $(DEB_ORG)    $(DEB_ARCHIVE)
SECDEB_ORG_SITES += $(SECDEB_ORG_US) $(SECDEB_ORG) $(SECDEB_ARCHIVE)
    SF_NET_SITES += $(SF_NET_EUROPE) $(SF_NET_US) $(SF_NET_AU)
APACHE_ORG_SITES += $(APACHE_ORG)
SRCARCHIVE_SITES += $(SRCARCHIVE_XWIRE)


endif # xwmake/defs/mirrors.mk
