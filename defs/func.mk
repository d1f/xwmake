ifndef	xwmake/defs/func.mk
	xwmake/defs/func.mk = included


# List of package files (*.make) in package dir
# usage: $(call pkg_files_f,PKGES_DIR)
pkg_files_f = $(wildcard $(1)/*.make) \
              $(wildcard $(1)/*/*.make)

# List of packages in package dir
# usage: $(call pkges_f,PKGES_DIR)
pkges_f = $(basename $(notdir $(call pkg_files_f,$(1)) ))

# Find package file (PKG.make) in all of package dirs by name
# usage: $(call pkg_file_f,PKG)
# returns full package file name in one of 'pkg' dirs in $(PKG_SEARCH_PATH)
pkg_file_f = $(firstword \
	$(foreach pkgdir,$(PKG_SEARCH_PATH),\
		$(or $(wildcard $(pkgdir)/$(1).make), \
		     $(wildcard $(pkgdir)/$(1)/$(1).make) \
		) \
	) \
)

# usage: $(call mul_sets2_f, set1, set2, apply_f)
mul_sets2_f = $(foreach item1,$(1),$(foreach \
		item2,$(2),$(call $(3),$(item1),$(item2))))

# usage: $(call mul_sets2_concat_f,list1,list2)
_concat2_f = $(1)$(2)
mul_sets2_concat_f = $(call mul_sets2_f,$(1),$(2),_concat2_f)


# usage: $(call lnsdir_f,DIR,TARGET,LINK)
lnsdir_f = cd $(1) && { rm -f $(3); ln -sf $(2) $(3); }


# most generic stamp function.
# Parameters: PLATFORM, PACKAGE, makeTARGET
stamp_f =	\
  $(if $(filter      $(SRC_ALL_TARGETS),$(strip $(3))),$(_ST_DIR)/src/$(strip $(2)).$(strip $(3)), \
  $(if $(filter    $(PATCH_ALL_TARGETS),$(strip $(3))),$(_ST_DIR)/$(1)/$(strip $(2)).$(strip $(3)), \
  $(if $(filter $(PLATFORM_ALL_TARGETS),$(strip $(3))),$(_ST_DIR)/$(1)/$(strip $(2)).$(strip $(3)), \
      $(error Unsupported make target $(3)) \
  )))

# makeTARGET, PACKAGE ...

# build platform stamp
# .. lang=ru
# ������� ������������ ���� ������ ��������� ������ ������� ��� ��������� "build".
bstamp_f = $(foreach p,$(2),$(call stamp_f,build,$(p),$(1)))

# target platform stamp
# .. lang=ru
# ������� ������������ ���� ������ ��������� ������ ������� ��� ������� ���������.
tstamp_f = $(foreach p,$(2),$(call stamp_f,$(PLATFORM),$(p),$(1)))


# 1 - PKG, 2 - FETCH_TYPE
# $(call found_file_f, pkg, main|patch)
found_file_f = $(SRC_ST_DIR)/$(1).$(2).found


endif # xwmake/defs/func.mk
