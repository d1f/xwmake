ifndef	xwmake/defs/prog.mk
	xwmake/defs/prog.mk = included


# .. lang=ru
# ��� ��������� make � ������� �����������.
MAKE    := $(MAKE) -R --no-print-directory

# .. lang=ru
# ��� �������� ������������
SHELL   := /bin/sh

# .. lang=ru
# ������� �������� ������ � ����������� ������ ������
RM      ?= rm -f

# .. lang=ru
# ������� ������������ �������� ������ � ��������� � ����������� ������ ������.
RM_R    ?= $(RM) -r

# .. lang=ru
# ������� �������� ������.
LN      ?= ln

# .. lang=ru
# ������� �������� ������������� ������.
LN_S    ?= $(LN) -sf

# .. lang=ru
# ������� �������� ���������.
MKDIR   ?= mkdir

# .. lang=ru
# ������� �������� ��������� � ��������� ������������� ���������
# � ��� ������ "��� ����������".
MKDIR_P ?= $(MKDIR) -p

# .. lang=ru
# ������� �������� ���������.
RMDIR   ?= rmdir

# .. lang=ru
# ������� �������� ���������, ������� ������������.
RMDIR_P ?= $(RMDIR) -p

# .. lang=ru
# ��� ��� ���� ��������� ���������� �������� - subversion.
SVN ?= svn
#SVN = $(BINST_DIR)/bin/svn

# .. lang=ru
# ������� �������� ��������� ������� ������.
XWMAKE_SCRIPT_DIR := $(XWMAKE_DIR)/script

# .. lang=ru
# ��������� �������� ������ � �������� �������� ��������� ������.
# ������������ ��� �������� ������ ��������� ������ ������.
TOUCH      := $(XWMAKE_SCRIPT_DIR)/touch

MLN        := $(XWMAKE_SCRIPT_DIR)/mln
MLN_S      := $(MLN) -s

# .. lang=ru
# ���������  �������� ��������� �������� �������� ������ �����������.
LNTREE     := $(XWMAKE_SCRIPT_DIR)/link-tree --skip.git

# .. lang=ru
# ��������� ������ � ������ ��������� ����� ������ (st_nlink).
LN_CNT := $(XWMAKE_SCRIPT_DIR)/link-count

# .. lang=ru
# ���������  ������������� ���������� ����� � ��������� �������� ������.
RELINK     := $(XWMAKE_SCRIPT_DIR)/relink-file

# .. lang=ru
# ��������� ��������� ������ ����� � �������� ������ ��� �����.
# ������� ��� ����������� ������������� ��������.
INST_FILE  := $(XWMAKE_SCRIPT_DIR)/inst_file

# .. lang=ru
# ��������� ��������� ��������� ������ � �������� �������.
# ������� ��� ����������� ������������� ��������.
INST_FILES := $(XWMAKE_SCRIPT_DIR)/inst_files

# .. lang=ru
# ��������� ��������� ������ ������������ ����� � �������� ������ ��� �����.
# ������� ��� ����������� ������������� ��������.
INSTxFILE  := $(XWMAKE_SCRIPT_DIR)/instXfile

# .. lang=ru
# ��������� ��������� ��������� ����������� ������ � �������� �������.
# ������� ��� ����������� ������������� ��������.
INSTxFILES := $(XWMAKE_SCRIPT_DIR)/instXfiles

# .. lang=ru
# ��������� ��������� � ������ �������� ������,
# �������� ������ � ��������� ����������.
CHMODALL   := $(XWMAKE_SCRIPT_DIR)/chmodall

# .. lang=ru
# ��������� ����������� ������� ����� �� ����������� ����� � �������� ����.
# �������� ������ ������������ ������������� �� ���������� �����.
ZCAT       := $(XWMAKE_SCRIPT_DIR)/zcat

# .. lang=ru
# ��������� �������� ������� �����.
# �������� ������ ������������ ������������� �� ���������� �����.
ZCHECK       := $(XWMAKE_SCRIPT_DIR)/zcheck

# .. lang=ru
# ��������� ��������� ����� ������.
VERCMP     := $(XWMAKE_SCRIPT_DIR)/versioncmp

# .. lang=ru
# ��������� �������� ����� � �������� ������� ������.
SUMCOL     := $(XWMAKE_SCRIPT_DIR)/sumcol


SED := $(or $(wildcard $(BBIN_DIR)/sed),$(if $(shell which gsed 2>/dev/null),gsed,sed))
AWK := $(or $(wildcard $(BBIN_DIR)/gawk),$(shell which gawk 2>/dev/null))
M4  := $(or $(wildcard $(BBIN_DIR)/m4),$(shell which m4 2>/dev/null))

FIND := $(if $(shell which gfind),gfind,find)

TAR := $(if $(shell which gtar),gtar,tar)


# use $(DOMAKE) to run foreign makefiles without passing down
# our own strict options

DOMAKE_VARS  = DEFS RULES
DOMAKE_VARS += TOP_DIR XWMAKE_DIR DEPOT_DIR VAR_DIR COMMON_DIR PRJ_DIR PRJ_PKG_DIR
DOMAKE_VARS += TOOLS_DIR TOOLS_PKG_DIR
DOMAKE_VARS += PKG PKG_DIR PKG_NAME PKG_VERSION
DOMAKE_VARS += PLATFORM LABEL
DOMAKE_VARS += MAKEFLAGS MAKELEVEL MAKEOVERRIDES MFLAGS INDENTS
DOMAKE_VARS += BINUTILS_VERSION GCC_VERSION LIBC_TYPE LIBC_VERSION LK_VERSION
DOMAKE_VARS += TC_OLEVEL TC_OFLAG TC_CFLAGS
DOMAKE_VARS += MK_JOBS INST_IMAGES_TO
DOMAKE_VARS += CCACHE

UNSET_MAKE_VARS = unset $(DOMAKE_VARS)

make := $(if $(shell which gmake),gmake,make)

# .. lang=ru
# ������� make ��� ��������� Makefile ������ � ����������� �����
# ����������, ���������� � ����������� :make:var:`ENV_VARS`.
DOMAKE = $(UNSET_MAKE_VARS); $(ENV_VARS) $(make)

# use $(IWMAKE) instead of $(IW[SH]) $(DOMAKE)
# .. lang=ru
# :make:var:`DOMAKE` � ������������� ��������������� ������.
IWMAKE = $(UNSET_MAKE_VARS); $(ENV_VARS) $(IW) $(make)


# for use in rules scripts
DOTGTMAKE =  $(ENV_VARS) $(make)
DOTGTMAKE += DEFS=$(DEFS) RULES=$(RULES)
DOTGTMAKE += TOP_DIR=$(TOP_DIR)
DOTGTMAKE += XWMAKE_DIR=$(XWMAKE_DIR) DEPOT_DIR=$(DEPOT_DIR)
DOTGTMAKE +=    VAR_DIR=$(VAR_DIR)   COMMON_DIR=$(COMMON_DIR)
DOTGTMAKE +=    PRJ_DIR=$(PRJ_DIR)  PRJ_PKG_DIR=$(PRJ_PKG_DIR)
DOTGTMAKE += LK_VERSION=$(LK_VERSION)
DOTGTMAKE += INDENTS=$(INDENTS1)
DOTGTMAKE += PLATFORM=$(PLATFORM)

DOBLDMAKE =  $(DOTGTMAKE) PLATFORM=build install


endif # xwmake/defs/prog.mk
