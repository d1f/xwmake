ifndef	xwmake/defs/pkg.mk
	xwmake/defs/pkg.mk = included


# control variables optionally supplied by package makefile:

# .. lang=ru
# ��� �������� ������� ������, ������������� � ������ �������� ������� ������.
# ��������. ������������ � ������ �������.
# �������� �� ���������: :make:var:`PKG_BASE`
PKG_DIR           ?= $(PKG_BASE)

# .. lang=ru
# ����� ������������ � ������ �������� ������� ������,
# ������� ����� ���������� ��� ����������.
# �������� �� ���������: 1.
EXTRACT_STRIP_NUM ?= 1
#EXTRACT_FILES


# .. lang=ru
# ��� ������.
# ��������:
#     *	ORIGIN: �������� �����, ����������� �� ����� ������������ (upstream).
#	��� �������, �������� � tar, ������ ����� �� ������������ (gzip, bzip2, xz).
#
#     *	DEBIAN1: ����� Debian � ������� 1.0, ����������� �� ����� �� ������ Debian.
#	������� ��:
#	  * ����� ������ �������� �������, �������� ����� <�����>_<������>.orig.tar.gz.
#	  * ��������� � ���� ����� <�����>_<������>-<��������>.diff.gz.
#	    ��������� ����� ����� ��������� ����� ��������� � ��������� ��������.
#
#     *	DEBIAN3: ����� Debian � ������� 3.0, ����������� �� ����� �� ������ Debian.
#	������� ��:
#	  * ����� ������ �������� �������, �������� ����� <�����>_<������>.orig.tar.gz.
#	  * ��������� � ���� ����� <�����>_<������>-<��������>.debian.tar.gz.
#	    ��������� ����� ����� ��������� ����� ��������� � ������� quilt.
#
#     *	DEBIAN_NATIVE: ����� Debian, ����������� �� ����� �� ������ Debian.
#	������� �� ����� ������ �������� �������, �������� ����� <�����>_<������>.tar.gz.
#	��������� ���.

PKG_TYPE ?= ORIGIN

ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)
  PKG_BASE ?= $(PKG)$(PKG_VER_MAJOR)
else
# .. lang=ru
# ������ ��� ������.
# �������� �� ���������: :make:var:`PKG`.
  PKG_BASE ?= $(PKG)
endif

ifndef PKG_VERSION
  ifneq  ($(strip $(PKG_VER_MAJOR).$(PKG_VER_MINOR)),.)
# .. lang=ru
# ������ ������.
# �������� �� ���������: :make:var:`PKG_VER_MAJOR`.:make:var:`PKG_VER_MINOR`
    PKG_VERSION = $(PKG_VER_MAJOR).$(PKG_VER_MINOR)
  endif
endif


ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)
  PKG_INFIX ?= _
else
# .. lang=ru
# ����������� ����� :make:var:`PKG_BASE` � :make:var:`PKG_VERSION`
# ��� ������������ :make:var:`PKG_NAME`.
  PKG_INFIX ?= -
endif

# .. lang=ru
# ������ ��� ������.
# �������� �� ���������: :make:var:`PKG_BASE`:make:var:`PKG_INFIX`:make:var:`PKG_VERSION`
PKG_NAME ?= $(if $(PKG_VERSION),$(PKG_BASE)$(PKG_INFIX)$(PKG_VERSION),$(PKG_BASE))


ifdef PKG_CVSROOT
  PKG_CVSDIR  ?= $(PKG_NAME)
  PKG_UPDATEABLE = defined

  ifneq ($(strip $(PKG_CVSTAG)),)
    ifneq ($(strip $(PKG_CVSTAG)),HEAD)
      pkg_CVSTAG += -r $(PKG_CVSTAG)
    endif
  endif
  ifneq ($(strip $(PKG_CVSDATE)),)
    pkg_CVSTAG += -D $(PKG_CVSDATE)
  endif
  ifeq ($(strip $(pkg_CVSTAG)),)
    pkg_CVSTAG = -A
  endif
  ifneq ($(strip $(PKG_CVSCODIR)),)
    pkg_CVSCODIR = -d $(PKG_CVSCODIR)
    PKG_DIR ?= $(PKG_CVSCODIR)
  endif
endif



ifdef PKG_SVNROOT
  PKG_SVNDIR  ?= $(PKG_NAME)
  PKG_UPDATEABLE = defined
  ifneq ($(strip $(PKG_SVNTAG)),)
    pkg_SVNTAG += -r $(PKG_SVNTAG)
  endif
  ifneq ($(strip $(PKG_SVNDATE)),)
    pkg_SVNTAG += -r "{$(PKG_SVNDATE)}"
  endif
  ifneq ($(strip $(PKG_SVNCODIR)),)
    pkg_SVNCODIR = $(PKG_SVNCODIR)
    PKG_DIR ?= $(PKG_SVNCODIR)
  endif
endif

ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_SRC_FILE_SUFFIX      ?= .orig.tar.gz
  PKG_LOCAL__FILE_SUFFICES ?= .orig.tar.gz
  PKG_REMOTE_FILE_SUFFICES ?= .orig.tar.gz
else ifeq ($(PKG_TYPE),DEBIAN3)
  PKG_SRC_FILE_SUFFIX      ?= .orig.tar.gz
  PKG_LOCAL__FILE_SUFFICES ?= .orig.tar.gz .orig.tar.bz2 .orig.tar.xz
  PKG_REMOTE_FILE_SUFFICES ?= .orig.tar.xz .orig.tar.bz2 .orig.tar.gz
else ifeq ($(PKG_TYPE),DEBIAN_NATIVE)
  PKG_SRC_FILE_SUFFIX      ?= .tar.gz
  PKG_LOCAL__FILE_SUFFICES ?= .tar.gz .tar.bz2 .tar.xz
  PKG_REMOTE_FILE_SUFFICES ?= .tar.xz .tar.bz2 .tar.gz
else
# .. lang=ru
# ��������� ������� ����� ����� ������ ������.
# �������� �� ���������: .tar.gz
  PKG_SRC_FILE_SUFFIX      ?= .tar.gz
  PKG_LOCAL__FILE_SUFFICES ?= .tar.gz .tar.bz2 .tar.xz .tgz .tbz .tbz2 .txz .tar
# .. lang=ru
# ������ ��������� ������� ����� ����� ������ ������, ������������ � ���������.
# ����� ���������, ���� ����� ��������, ��� ������� � ������� ����������� ���.
  PKG_REMOTE_FILE_SUFFICES ?= .tar.xz .tar.bz2 .tar.gz  #.txz .tbz2 .tbz .tgz
endif

# .. lang=ru
# �������� ����� ����� ����� ������.
# �������� �� ���������: :make:var:`PKG_NAME`.
PKG_FILE_BASE ?= $(PKG_NAME)

# .. lang=ru
# ������ ��� ����� ������ ������.
# �������� �� ���������: :make:var:`PKG_FILE_BASE`:make:var:`PKG_SRC_FILE_SUFFIX`
PKG_SRC_FILE ?= $(PKG_FILE_BASE)$(PKG_SRC_FILE_SUFFIX)

ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)
  PKG_SITES ?= $(DEB_ORG_SITES)
endif
#PKG_SITE_PATH


# .. lang=ru
# ������� �������� ������� ������ ����� ����������.
ifeq ($(PKG_FETCH_METHOD),DONE)
   PKG_ORIG_SRC_DIR ?=    $(PKG_ORIG_DIR)
else
   PKG_ORIG_SRC_DIR ?=    $(ORIG_SRC_DIR)/$(PKG_DIR)$(if $(PKG_VERSION),_$(PKG_VERSION))
endif

# .. lang=ru
# ������� �������� ������� ������ � ����������� ���������.
PKG_PATCHED_SRC_DIR ?= $(PATCHED_SRC_DIR)/$(PKG_DIR)$(if $(PKG_VERSION),_$(PKG_VERSION))


#FXIME: replace to PKG_PATCHED_SRC_DIR
# .. lang=ru
# �������� ��� ��� :make:var:`PKG_PATCHED_SRC_DIR`
PKG_SRC_DIR ?= $(PKG_PATCHED_SRC_DIR)

#FXIME: replace to PKG_PATCHED_SRC_SUB_DIR
# .. lang=ru
# ���������� � �������� �������� ������� ������ � ����������� ���������.
PKG_SRC_SUB_DIR ?= $(PKG_PATCHED_SRC_DIR)/$(PKG_SUB_DIR)

# .. lang=ru
# ������� ������ �������� ������� ������.
PKG_BLD_DIR ?= $(BLD_DIR)/$(PKG_DIR)

# .. lang=ru
# ���������� � �������� ������ �������� ������� ������.
PKG_BLD_SUB_DIR ?= $(PKG_BLD_DIR)/$(PKG_SUB_DIR)


ifeq ($(PKG_TYPE),DEBIAN1)
  fetch.patch: PKG_FILE_BASE            = $(PKG_NAME)$(PKG_VER_PATCH)
  fetch.patch: PKG_SRC_FILE_SUFFIX      = .diff.gz
  fetch.patch: PKG_LOCAL__FILE_SUFFICES = .diff.gz
  fetch.patch: PKG_REMOTE_FILE_SUFFICES = .diff.gz

  fetch: fetch.main fetch.patch
else ifeq ($(PKG_TYPE),DEBIAN3)
  fetch.patch: PKG_FILE_BASE            = $(PKG_NAME)$(PKG_VER_PATCH)
  fetch.patch: PKG_SRC_FILE_SUFFIX      = .debian.tar.gz
  fetch.patch: PKG_LOCAL__FILE_SUFFICES = .debian.tar.xz .debian.tar.bz2 .debian.tar.gz
  fetch.patch: PKG_REMOTE_FILE_SUFFICES = .debian.tar.xz .debian.tar.bz2 .debian.tar.gz

  fetch: fetch.main fetch.patch
endif

# -pNUM --strip=NUM
# .. lang=ru
# ����� ������������ � ������ ������ ������,
# ������� ����� ���������� ��� ����������.
# �������� �� ���������: 1.
PKG_PATCH_STRIP_NUM ?= 1

# .. lang=ru
# ����� ������������ � ������ ������ ������ ������ ������,
# ������� ����� ���������� ��� ����������.
# �������� �� ���������: :make:var:`PKG_PATCH_STRIP_NUM`.
PKG_PATCH2_STRIP_NUM ?= $(PKG_PATCH_STRIP_NUM)

# .. lang=ru
# ����� ������������ � ������ ������� ������ ������ ������,
# ������� ����� ���������� ��� ����������.
# �������� �� ���������: :make:var:`PKG_PATCH_STRIP_NUM`.
PKG_PATCH3_STRIP_NUM ?= $(PKG_PATCH_STRIP_NUM)

ifndef PKG_HAS_NO_SOURCES
  ifeq ($(PKG_TYPE),DEBIAN1)
# .. lang=ru
# ������ ������ ������ ������.
    PKG_PATCH_FILES ?= $(DL_DIR)/$(PKG_NAME)$(PKG_VER_PATCH).diff.gz
  else ifeq ($(PKG_TYPE),DEBIAN3)
    #PKG_PATCH_FILES ?= $(DL_DIR)/$(PKG_NAME)$(PKG_VER_PATCH).debian.tar.gz
  endif
endif


# PKG_FETCH_METHOD:

# NGET | CVS | SVN | DONE
# DONE - already downloaded/check-outed, don't fetch; Updatable.

ifdef PKG_SITES
# .. lang=ru
# ������ ��������� ������ �������� ������� ������.
# ��������� ��������: NGET | CVS | SVN | DONE
      PKG_FETCH_METHOD ?= NGET
endif
ifdef PKG_URL
      PKG_FETCH_METHOD ?= NGET
endif

ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)
      PKG_FETCH_METHOD ?= NGET
endif

ifdef PKG_CVSROOT
      PKG_FETCH_METHOD ?= CVS
      PKG_UPDATEABLE = defined
endif

ifdef PKG_SVNROOT
      PKG_FETCH_METHOD ?= SVN
      PKG_UPDATEABLE = defined
endif

ifndef  PKG_HAS_NO_SOURCES
ifndef  PKG_FETCH_METHOD
$(error PKG_FETCH_METHOD undefined)
endif

ifneq ($(strip $(filter-out NGET CVS SVN DONE,$(PKG_FETCH_METHOD))),)
$(error unknown PKG_FETCH_METHOD $(PKG_FETCH_METHOD))
endif
endif

# .. lang=ru
# ���� � �������� ������� ��� �������� �� ������ ������.
# �������� �� ���������: .link-count
PKG_LN_CNT_FILE ?= .link-count


# .. lang=ru
# ���������� PKG_SUB_DIR � PKG_ORIG_DIR
PKG_ORIG_SUB_DIR ?= $(PKG_ORIG_DIR)/$(PKG_SUB_DIR)


endif # xwmake/defs/pkg.mk
