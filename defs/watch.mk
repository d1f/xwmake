ifndef	xwmake/defs/watch.mk
	xwmake/defs/watch.mk = included


.PHONY: watch%

ifeq ($(PKG_FETCH_METHOD),NGET)


PKG_WATCHFILE = $(VAR_DIR)/watch/$(PKG).watch

watch: watchfile

watchfile: $(PKG_WATCHFILE)

$(PKG_WATCHFILE): $(VAR_DIR)/watch/.dir
$(PKG_WATCHFILE): $(call pkg_file_f,$(PKG))
$(PKG_WATCHFILE): $(XWMAKE_DIR)/defs/watch.mk $(XWMAKE_DIR)/rules/watch.mk

ifeq ($(PKG_TYPE),ORIGIN)
  PKG_WATCH_PATTERN = $(PKG_BASE)$(PKG_INFIX)([\.\d]+)($(subst $(SPACE),|,$(strip \
			$(subst .,\.,$(PKG_REMOTE_FILE_SUFFICES)))))
else ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)
  PKG_WATCH_PATTERN = $(PKG_BASE)_(.+).dsc
endif

PKG_SITES_WATCH = $(if $(findstring DEBIAN,$(PKG_TYPE)), $(wordlist 1,3,$(PKG_SITES)), $(PKG_SITES))


else


watch%:
	@echo "$(I)$(PKG) is not downloadable package!"
	@exit 1


endif # $(PKG_FETCH_METHOD) == NGET


endif # xwmake/defs/watch.mk
