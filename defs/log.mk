ifndef	xwmake/defs/log.mk
	xwmake/defs/log.mk = included


# stamp targets log files:
# .. lang=ru
# ���� ������� � ����������� ������� ���������
 LOG_FILE = $(LOG_DIR)/$(PKG).$(patsubst .%,%,$(suffix $@)).log
PLOG_FILE = $(LOG_DIR)/$(PKG).$@.log

# stamp targets log redirect:
# .. lang=ru
# ��������������� ������� ����������� ������ � ������ � ���� �������
# :make:var:`LOG_FILE`.
 LOG = 1>> $(LOG_FILE) 2>> $(LOG_FILE)

# phony targets log redirect:
PLOG = 1>> $(PLOG_FILE) 2>> $(PLOG_FILE)

# .. lang=ru
# ����������� ������ ������� ��������� � ������ �������.
define LOG_SEPARATOR
 ==============================================================================
endef

# .. lang=ru
# ������� ���������� ������ ������� ������������ ������ ������� ���������
# :make:var:`LOG_SEPARATOR`.
define APPEND_LOG_SEPARATOR_CMD
  echo "$(LOG_SEPARATOR)" >> $(LOG_FILE)
endef


endif # xwmake/defs/log.mk
