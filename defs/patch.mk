ifndef	xwmake/defs/patch.mk
	xwmake/defs/patch.mk = included


ifeq ($(PKG_TYPE),DEBIAN3)

include $(TOOLS_PKG_DIR)/quilt.mk

define PATCH_CMD
  echo -n "$(I2)Extracting debian part ... "
  $(ZCAT) $(DL_DIR)/`cat $(FOUND_FILE_PATCH)` | \
  $(TAR) -C $(PKG_PATCHED_SRC_DIR) -xvf - $(LOG)
  echo "done"

  $(APPLY_QUILT_PATCHES_CMD)
endef
endif


define APPLY_DPATCHES_CMD
  echo "$(I2)Fixing /usr/share/dpatch/ -> $(BSHARE_DIR)/dpatch/"
  $(TOOLS_PKG_DIR)/dpatch/dpatch-fix.sh  $(BINST_DIR)	\
	$(PKG_PATCHED_SRC_DIR)/debian/patches/*			      $(LOG)

  echo "$(I2)Applying dpatches ..."
  cd $(PKG_PATCHED_SRC_DIR) && \
	DEB_BUILD_ARCH=$(BUILD_ARCH) dpatch --strict apply-all        $(LOG)
endef


endif # xwmake/defs/patch.mk
