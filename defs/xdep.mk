# cross package dependency generation rules:
ifndef	xwmake/defs/xdep.mk
	xwmake/defs/xdep.mk = included


define _xdep_usage_

ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
endif # XDEP

endef # _xdep_usage_

# XDEP=1 ./b TARGET PKG


# FIXME: PLATFORM depended
XDEP_DIR = $(_ST_DIR)/xdep
PKG_XDEP = $(XDEP_DIR)/$(PKG).xdep


#  @echo -n "$(I)Generating xdep for $(@F) ... "
#  @echo done
define XDEP_TARGET
  @$(if $^,echo "$@ : $^" >> $(PKG_XDEP))
endef


endif # xwmake/defs/xdep.mk
