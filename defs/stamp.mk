ifndef	xwmake/defs_stamp.mk
	xwmake/defs_stamp.mk = included


	# main | patch
         FETCH_TYPE = $(lastword $(subst ., ,$@))
        FETCH_STAMP = $(call stamp_f,src,$(PKG),fetch)
      UNFETCH_STAMP = $(call stamp_f,src,$(PKG),unfetch)
   FETCH_MAIN_STAMP =   $(FETCH_STAMP).main
 UNFETCH_MAIN_STAMP = $(UNFETCH_STAMP).main
  FETCH_PATCH_STAMP =   $(FETCH_STAMP).patch
UNFETCH_PATCH_STAMP = $(UNFETCH_STAMP).patch
        CHECK_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),check)
      UNCHECK_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),uncheck)
      EXTRACT_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),extract)
    UNEXTRACT_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),unextract)
        PATCH_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),patch)
      UNPATCH_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),unpatch)
         LINK_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),link)
       UNLINK_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),unlink)
PRE_CONFIGURE_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),pre-configure)
    CONFIGURE_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),configure)
       DEPEND_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),depend)
     UNDEPEND_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),undepend)
        BUILD_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),build)
      UNBUILD_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),unbuild)
      INSTALL_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),install)
    UNINSTALL_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),uninstall)
  INSTALL_ROOT_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),install-root)
UNINSTALL_ROOT_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),uninstall-root)
         PACK_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),pack)
       UNPACK_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),unpack)
  PACK_STATUS_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),pack-status)
UNPACK_STATUS_STAMP = $(call stamp_f,$(PLATFORM),$(PKG),unpack-status)

   FOUND_FILE_MAIN  = $(call found_file_f,$(PKG),main)
   FOUND_FILE_PATCH = $(call found_file_f,$(PKG),patch)
   FOUND_FILE       = $(call found_file_f,$(PKG),$(FETCH_TYPE))


endif # xwmake/defs_stamp.mk
