ifndef	xwmake/defs/cmd.mk
	xwmake/defs/cmd.mk = included


# .. lang=ru
# �������� ��� ���� \*_CMD_DEFAULT - ���� � ��� ������������ ����� make.
# �������� �� ���������: <�����>.
# �� ������ � MAKEFILES
MAKEFILE ?=

# .. lang=ru
# �������� ��� :make:var:`DEPEND_CMD_DEFAULT` - make target.
# �������� �� ���������: depend.
    DEPEND_CMD_TARGET ?= depend

# .. lang=ru
# �������� ��� :make:var:`BUILD_CMD_DEFAULT` - make target.
# �������� �� ���������: all.
     BUILD_CMD_TARGET ?= all

# .. lang=ru
# �������� ��� :make:var:`INSTALL_CMD_DEFAULT` - make target.
# �������� �� ����������: install.
   INSTALL_CMD_TARGET ?= install

   INSTALL_ROOT_CMD_TARGET ?= install-root

# .. lang=ru
# �������� ��� :make:var:`CLEAN_CMD_DEFAULT` - make target.
# �������� �� ���������: clean.
     CLEAN_CMD_TARGET ?= clean

# .. lang=ru
# �������� ��� :make:var:`DISTCLEAN_CMD_DEFAULT` - make target.
# �������� �� ���������: distclean.
 DISTCLEAN_CMD_TARGET ?= distclean


# .. lang=ru
# ����� ������������ ��������� ������ ������.
# ������������ :ref:`�������� ������� ��������� <b-project>`.
# ������������ :ref:`�������� ��������� ��������� <xwmake-setup>`
# �� ����� :ref:`��������� ��������� <��������� ���������>`
# ������� ����ޣ��� ����� ����������� �����������
# (���������� � �����������) � /proc/cpuinfo.
# ��������� MK_JOBS = 1 � ������ ��� ������� ������������ ������ ������.
MK_JOBS ?= 1
#FIXME: �������� ���� ����� �� ������ �� reference-manual/control-vars.ru MK_JOBS


# .. lang=ru
# �������� ��� make �� :make:var:`MK_JOBS`
MKJ = -j$(MK_JOBS)

# .. lang=ru
# ��������� ��� �������, ����������� ������� ���� "depend".
    DEPEND_CMD_DEFAULT = $(DOMAKE)        -C $(PKG_BLD_SUB_DIR) $(if $(MAKEFILE),-f $(MAKEFILE))    $(DEPEND_CMD_TARGET) $(MK_VARS)   $(DEPEND_MK_VARS)  $(LOG)

# .. lang=ru
# ��������� ��� �������, ����������� ������� ���� "build".
     BUILD_CMD_DEFAULT = $(DOMAKE) $(MKJ) -C $(PKG_BLD_SUB_DIR) $(if $(MAKEFILE),-f $(MAKEFILE))     $(BUILD_CMD_TARGET) $(MK_VARS)    $(BUILD_MK_VARS)  $(LOG)

# .. lang=ru
# ��������� ��� �������, ����������� ������� ���� "install".
   INSTALL_CMD_DEFAULT = $(IWMAKE)        -C $(PKG_BLD_SUB_DIR) $(if $(MAKEFILE),-f $(MAKEFILE))   $(INSTALL_CMD_TARGET) $(MK_VARS)  $(INSTALL_MK_VARS)  $(LOG)

   INSTALL_ROOT_CMD_DEFAULT = $(IWMAKE)        -C $(PKG_BLD_SUB_DIR) $(if $(MAKEFILE),-f $(MAKEFILE))   $(INSTALL_ROOT_CMD_TARGET) $(MK_VARS)  $(INSTALL_MK_VARS)  $(LOG)

# .. lang=ru
# ��������� ��� �������, ����������� ������� ���� "clean".
     CLEAN_CMD_DEFAULT = $(DOMAKE)        -C $(PKG_BLD_SUB_DIR) $(if $(MAKEFILE),-f $(MAKEFILE))     $(CLEAN_CMD_TARGET) $(MK_VARS)    $(CLEAN_MK_VARS)  $(LOG)

ifndef SEPARATE_SRC_BLD_DIRS
# .. lang=ru
# ��������� ��� �������, ����������� ������� ���� "distclean".
 DISTCLEAN_CMD_DEFAULT = $(DOMAKE)        -C $(PKG_BLD_SUB_DIR) $(if $(MAKEFILE),-f $(MAKEFILE)) $(DISTCLEAN_CMD_TARGET) $(MK_VARS) $(DISTCLEANMK_VARS) $(LOG)
else
 DISTCLEAN_CMD_DEFAULT = $(RM_R) $(PKG_BLD_DIR)
endif


endif # xwmake/defs/cmd.mk
