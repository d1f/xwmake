ifndef	xwmake/defs/util.mk
	xwmake/defs/util.mk = included


# .. lang=ru
# ���������� � ������ �������
EMPTY:=

# .. lang=ru
# ���������� � ��������
SPACE:=$(EMPTY) $(EMPTY)

# .. lang=ru
# ���������� � �������� ���������
SPACE4:=$(EMPTY)    $(EMPTY)

SPACE2:=$(EMPTY)  $(EMPTY)

# .. lang=ru
# ���������� � �����������
TAB:=$(EMPTY)	$(EMPTY)

# .. lang=ru
# ���������� � �������
COMMA:=,

# .. lang=ru
# ���������� � ��������� ��������.
SQUOTE:='

# Escape single quote for use in echo statements
# .. lang=ru
# ���������� � �������������� ��������� ��������.
# ��� ������������� � echo.
ESCSQ = $(subst $(SQUOTE),'\$(SQUOTE)',$1)

INDENTS1=$(INDENTS)i
INDENTS2=$(INDENTS1)i
INDENTS3=$(INDENTS2)i

include $(XWMAKE_DIR)/indent-spaces

# .. lang=ru
# ����� 0-�� ������.
I  = $(subst i,"$(INDENT_SPACES)",$(INDENTS))

# .. lang=ru
# ����� 1-�� ������.
I1 = $(subst i,"$(INDENT_SPACES)",$(INDENTS)i)

# .. lang=ru
# ����� 2-�� ������.
I2 = $(subst i,"$(INDENT_SPACES)",$(INDENTS)ii)

# .. lang=ru
# ����� 3-�� ������.
I3 = $(subst i,"$(INDENT_SPACES)",$(INDENTS)iii)


endif # xwmake/defs/util.mk
