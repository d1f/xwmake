# short target aliases
ifndef	xwmake/rules/alias.mk
	xwmake/rules/alias.mk = included


.PHONY		: pre-conf pre-conf-log
pre-conf	: pre-configure
pre-conf-log	: pre-configure-log

.PHONY		: conf conf-log conf-help conf-help-rec
conf		: configure
conf-log	: configure-log
conf-help	: configure-help
conf-help-rec	: configure-help-recursive

.PHONY		: bld unbld rebld bld-log
bld		: build
bld-log		: build-log
unbld		: unbuild
rebld		: rebuild

.PHONY		: dep dep-log dep-clean depclean undepend undep redepend redep
dep		: depend
dep-log		: depend-log
dep-clean depclean undepend undep : depend-clean
redep		: redepend

.PHONY		: inst uninst reinst
      inst	:   install
    uninst	: uninstall
    reinst	: reinstall
      inst-log	:   install-log
    uninst-log	: uninstall-log
    reinst-log	: reinstall-log

      inst-root	:   install-root
    uninst-root	: uninstall-root
    reinst-root	: reinstall-root
      inst-root-log :   install-root-log


.PHONY     : unfetchlog unpatchlog unlinklog uninstlog
unfetchlog : unfetch    unlog
unpatchlog : unpatch    unlog
 unlinklog : unlink     unlog
 uninstlog : uninst     unlog


.PHONY:     rsize rsize-sum
rsize     : root-size
rsize-sum : root-size-sum


endif # xwmake/rules/alias.mk
