ifndef	xwmake/rules/patch.mk
	xwmake/rules/patch.mk = included


pkg_patch_log = $(LOG_DIR)/$(PKG).patch.log2

$(PATCH_STAMP): $(EXTRACT_STAMP) $(PATCHED_SRC_DIR)/.dir $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNPATCH_STAMP)
ifndef PKG_HAS_NO_SOURCES
 ifndef PKG_NO_LINK
	@echo "$(I)Patching        $(PKG_BASE) $(PKG_VERSION)"
	@$(APPEND_LOG_SEPARATOR_CMD)

	@echo "$(I1)Linking         $(PKG_BASE) $(PKG_VERSION) sources for patching ... "
	@$(RM_R)			$(PKG_PATCHED_SRC_DIR) $(LOG)
	@$(LNTREE) $(PKG_ORIG_SRC_DIR)	$(PKG_PATCHED_SRC_DIR) $(LOG)

  ifneq ($(strip $(PKG_PATCH_COPYDIRS)),)
	@echo "$(I1)Copying files ... "
	@for d in $(PKG_PATCH_COPYDIRS); do \
		cd $$d && cp -Rv . $(PKG_PATCHED_SRC_DIR) $(LOG); \
	done
  endif # PKG_PATCH_COPYDIR defined

  ifneq ($(strip $(PKG_PATCH_FILES)),)
	@echo "$(I1)Relinking       $(PKG_BASE) $(PKG_VERSION) sources for patching ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-relink-cmd $(PKG_PATCH_STRIP_NUM)	\
					$(PKG_PATCHED_SRC_DIR)		\
					$(PKG_PATCH_FILES) $(LOG)

	@echo "$(I1)Patching        $(PKG_BASE) $(PKG_VERSION) ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-cmd	$(pkg_patch_log)		\
						$(PKG_PATCH_STRIP_NUM)	\
					$(PKG_PATCHED_SRC_DIR)		\
					$(PKG_PATCH_FILES) $(LOG)	\
	|| { cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log); exit 1; }
	@    cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log)
  endif # PKG_PATCH_FILES defined

  ifneq ($(strip $(PATCH_CMD)),)
	@echo "$(I1)Cmd-Patching    $(PKG_BASE) $(PKG_VERSION) ... "
	@$(PATCH_CMD)
  endif

  ifneq ($(strip $(PKG_PATCH2_COPYDIRS)),)
	@echo "$(I1)Copying files ... "
	@for d in $(PKG_PATCH2_COPYDIRS); do \
		cd $$d && cp -Rv . $(PKG_PATCHED_SRC_DIR) $(LOG); \
	done
  endif # PKG_PATCH2_COPYDIR defined

  ifneq ($(strip $(PKG_PATCH2_FILES)),)
	@echo "$(I1)Relinking       $(PKG_BASE) $(PKG_VERSION) sources for patching 2 ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-relink-cmd $(PKG_PATCH2_STRIP_NUM)	\
					$(PKG_PATCHED_SRC_DIR)		\
					$(PKG_PATCH2_FILES) $(LOG)

	@echo "$(I1)Patching 2      $(PKG_BASE) $(PKG_VERSION) ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-cmd	$(pkg_patch_log)		\
					$(PKG_PATCH2_STRIP_NUM)		\
					$(PKG_PATCHED_SRC_DIR)		\
					$(PKG_PATCH2_FILES) $(LOG)	\
	|| { cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log); exit 1; }
	@    cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log)
  endif # PKG_PATCH2_FILES defined

  ifneq ($(strip $(PATCH2_CMD)),)
	@echo "$(I1)Cmd-Patching 2  $(PKG_BASE) $(PKG_VERSION) ... "
	@$(PATCH2_CMD)
  endif

  ifneq ($(strip $(PKG_PATCH3_COPYDIRS)),)
	@echo "$(I1)Copying files ... "
	@for d in $(PKG_PATCH3_COPYDIRS); do \
		cd $$d && cp -Rv . $(PKG_PATCHED_SRC_DIR) $(LOG); \
	done
  endif # PKG_PATCH3_COPYDIR defined

  ifneq ($(strip $(PKG_PATCH3_FILES)),)
	@echo "$(I1)Relinking       $(PKG_BASE) $(PKG_VERSION) sources for patching 3 ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-relink-cmd $(PKG_PATCH3_STRIP_NUM)	\
					$(PKG_PATCHED_SRC_DIR)		\
					$(PKG_PATCH3_FILES) $(LOG)

	@echo "$(I1)Patching 3      $(PKG_BASE) $(PKG_VERSION) ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-cmd	$(pkg_patch_log)		\
					$(PKG_PATCH3_STRIP_NUM)		\
					$(PKG_PATCHED_SRC_DIR)		\
					$(PKG_PATCH3_FILES) $(LOG)	\
	|| { cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log); exit 1; }
	@    cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log)
  endif # PKG_PATCH3_FILES defined

  ifneq ($(strip $(PATCH3_CMD)),)
	@echo "$(I1)Cmd-Patching 3  $(PKG_BASE) $(PKG_VERSION) ... "
	@$(PATCH3_CMD)
  endif

	@echo "$(I1)Changing to R/O $(PKG_BASE) $(PKG_VERSION) ... "
	@chmod -R     a-w   $(PKG_PATCHED_SRC_DIR) $(LOG)

 else  # ndef PKG_NO_LINK
	@$(LN_S) $(PKG_ORIG_SRC_DIR) $(PKG_PATCHED_SRC_DIR) $(LOG)
 endif # ndef PKG_NO_LINK
endif #ndef PKG_HAS_NO_SOURCES
	@$(TOUCH) $@
endif # XDEP


unpatch patch-clean: $(UNPATCH_STAMP)

$(UNPATCH_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(PATCH_STAMP)
	@echo "$(I)Unpatching      $(PKG_BASE) $(PKG_VERSION) ... "
 ifndef PKG_NO_LINK
	@$(CHMODALL) u+w d	$(PKG_PATCHED_SRC_DIR) $(LOG)
	@-$(RM_R)		$(PKG_PATCHED_SRC_DIR) $(LOG)
	@if $(LN_CNT) check1 $(PKG_ORIG_SRC_DIR) $(PKG_LN_CNT_FILE); then \
	    chmod -f -R u+w  $(PKG_ORIG_SRC_DIR) || true; \
	fi
 else
	@$(RM) $(PKG_PATCHED_SRC_DIR) $(LOG)
 endif # ndef PKG_NO_LINK
	@$(TOUCH) $@
endif # XDEP


endif # xwmake/rules/patch.mk
