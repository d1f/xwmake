# debian/watch
ifndef	xwmake/rules/watch.mk
	xwmake/rules/watch.mk = included


ifeq ($(PKG_FETCH_METHOD),NGET)

uscan_opts = --no-download --skip-signature --no-dehs #--report-status

watch:
	@echo -n "$(I)Watching $(PKG) for version $(PKG_VERSION)$(PKG_VER_PATCH) ... "
	@if ! test -e $(PKG_WATCHFILE); then echo "$(I)No debian/watch found"; exit 1; fi
	@uscan --package $(PKG) --watchfile $(PKG_WATCHFILE) --upstream-version $(PKG_VERSION)$(PKG_VER_PATCH) $(uscan_opts); \
		rc=$$?; if test $$rc -eq 1; then echo "No new upstream versions found"; fi


$(PKG_WATCHFILE):
	@echo -n "$(I)Generating $(PKG) $(PKG_VERSION)$(PKG_VER_PATCH) watch file ... "
	@echo "version=4" > $@
	@echo 'opts="pgpmode=none,passive"' >> $@
	@for site in $(PKG_SITES_WATCH); do \
	     echo "$$site/$(PKG_SITE_PATH)$(TAB)$(PKG_WATCH_PATTERN)" >> $@; \
	done
	@echo "done"


watchfile-clean:
	@$(RM) $(PKG_WATCHFILE) $(LOG)


endif # $(PKG_FETCH_METHOD) == NGET


endif # xwmake/rules/watch.mk
