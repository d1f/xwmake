# Last build/target/platform symlinks:
ifndef	xwmake/rules/lastlink.mk
	xwmake/rules/lastlink.mk = included


LAST_LINK_SFX =
LAST_LINK_PFX = .lastlink.

# usage: $(call ll_stamp_f,TEXT)
ll_stamp_f = $(LAST_LINK_PFX)$(1)$(LAST_LINK_SFX)

# usage: $(call DIR,TARGET)
define link_last_f
	$(RM) $(1)/$(call ll_stamp_f,*) $(1)/last
	if test -d $(1); then $(call lnsdir_f,$(1),$(2),last) && touch $@; fi
endef

LAST_LINK_STAMPS  = $(VAR_DIR)/src/$(call ll_stamp_f,$(PLATFORM))
LAST_LINKS        = $(VAR_DIR)/src/last
$(VAR_DIR)/src/$(call ll_stamp_f,$(PLATFORM)) :
	@#echo "$(I)Symlinking \"last\" to $(PLATFORM) in $(dir $@) ... "
	@$(call link_last_f,$(dir $@),$(PLATFORM))

#VAR_VARIANT_DIRS =  $(ST_DIR)  $(LOG_DIR)  $(IWL_DIR)  $(BLD_DIR)  $(INST_DIR)  $(ROOT_DIR)
_VAR_VARIANT_DIRS = $(_ST_DIR) $(_LOG_DIR) $(_IWL_DIR) $(_BLD_DIR) $(_INST_DIR)
ifneq ($(PLATFORM),build)
_VAR_VARIANT_DIRS += $(_ROOT_DIR) # _ROOT_DIR == _INST_DIR for build platform
endif

LAST_LINK_STAMPS += $(foreach i,$(_VAR_VARIANT_DIRS),$(i)/$(call ll_stamp_f,$(PLATFORM)))
LAST_LINKS       += $(foreach i,$(_VAR_VARIANT_DIRS),$(i)/last)
$(foreach i,$(_VAR_VARIANT_DIRS),$(i)/$(call ll_stamp_f,$(PLATFORM))) :
	@#echo "$(I)Symlinking \"last\" to $(VARIANT) in $(dir $@) ... "
	@$(call link_last_f,$(dir $@),$(PLATFORM))

define _tool_dir_
LAST_LINK_STAMPS += $(_INST_DIR)/$(call ll_stamp_f,build)
LAST_LINKS       += $(_INST_DIR)/last
$(_INST_DIR)/$(call ll_stamp_f,build) :
	@#echo "$(I)Symlinking \"last\" to build in $(dir $@) ... "
	@$(call link_last_f,$(dir $@),build)

LAST_LINK_STAMPS += $(BINST_DIR)/$(call ll_stamp_f,$(PLATFORM))
LAST_LINKS       += $(BINST_DIR)/last
$(BINST_DIR)/$(call ll_stamp_f,$(PLATFORM)) :
	@#echo "$(I)Symlinking \"last\" to $(PLATFORM) in $(dir $@) ... "
	@$(call link_last_f,$(dir $@),$(PLATFORM))

LAST_LINK_STAMPS += $(BINST_DIR)/$(PLATFORM)/$(call ll_stamp_f,$(PLATFORM))
LAST_LINKS       += $(BINST_DIR)/$(PLATFORM)/last
$(BINST_DIR)/$(PLATFORM)/$(call ll_stamp_f,$(PLATFORM)) :
	@#echo "$(I)Symlinking \"last\" to $(PLATFORM) in $(dir $@) ... "
	@$(call link_last_f,$(dir $@),$(PLATFORM))
endef

.PHONY: lastlink lastlink-clean
lastlink	: $(LAST_LINK_STAMPS)
lastlink-clean	:
	@echo "$(I)Removing \"last\" symlinks ... "
	@$(RM)	  $(LAST_LINK_STAMPS) $(LAST_LINKS)

$(PLATFORM_TARGETS) $(PLATFORM_UNTARGETS) : lastlink


endif # xwmake/rules/lastlink.mk
