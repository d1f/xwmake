ifndef	xwmake/rules/root.mk
	xwmake/rules/root.mk = included


.PHONY: root-size root-size-sum
root-size :
	@$(LIST_INSTALL_FILES) | \
	xargs stat -c "%7s %n" 2>/dev/null | \
	sed -e 's,$(ROOT_DIR),,g'

root-size-sum :
	@$(LIST_INSTALL_FILES) | \
	xargs stat -c "%s" 2>/dev/null | \
	{ sum=`$(SUMCOL)`; printf "%7u $(PKG)\n" $$sum; }


iwl_pkges = $(basename $(notdir $(wildcard $(IWL_DIR)/*.iwl)))

root_pkges_cmd = \
	for p in $(iwl_pkges); do \
	    p_files=`$(LIST_INSTALL) $(IWL_DIR)/$$p.iwl 2>/dev/null | \
		egrep "^(file|dir ) $(ROOT_DIR)"`; \
	    if test -n "$$p_files"; then echo $$p; fi; \
	done

root_pkges_files_cmd = \
	for p in $(iwl_pkges); do \
	    p_files=`$(LIST_INSTALL) $(IWL_DIR)/$$p.iwl 2>/dev/null | \
		egrep "^file $(ROOT_DIR)"`; \
	    if test -n "$$p_files"; then echo $$p; fi; \
	done

root_targets  = root-uninst root-uninstall-root root-unlink root-unpatch root-unextract root-unfetch
root_targets += root-list

ifneq ($(filter $(root_targets),$(MAKECMDGOALS)),)
  root_list       := $(shell $(root_pkges_cmd))
else ifneq ($(filter %pack-root %pack-status-root list-root-files,$(MAKECMDGOALS)),)
  root_list_files := $(shell $(root_pkges_files_cmd))
endif

.PHONY : root-%
root-uninstall-root : $(foreach pkg, $(root_list), $(call stamp_f,$(PLATFORM),$(pkg),uninstall-root))
root-uninstall : $(foreach pkg, $(root_list), $(call stamp_f,$(PLATFORM),$(pkg),uninstall))
root-unlink    : $(foreach pkg, $(root_list), $(call stamp_f,$(PLATFORM),$(pkg),unlink))
root-unpatch   : $(foreach pkg, $(root_list), $(call stamp_f,$(PLATFORM),$(pkg),unpatch))
root-unextract : $(foreach pkg, $(root_list), $(call stamp_f,$(PLATFORM),$(pkg),unextract))
root-unfetch   : $(foreach pkg, $(root_list), $(call stamp_f,$(PLATFORM),$(pkg),unfetch))

root-pack          : $(foreach pkg, $(root_list_files), $(call stamp_f,$(PLATFORM),$(pkg),pack))
root-unpack        : $(foreach pkg, $(root_list_files), $(call stamp_f,$(PLATFORM),$(pkg),unpack))
root-pack-status   : $(foreach pkg, $(root_list_files), $(call stamp_f,$(PLATFORM),$(pkg),pack-status))
root-unpack-status : $(foreach pkg, $(root_list_files), $(call stamp_f,$(PLATFORM),$(pkg),unpack-status))

root-list :
	@for p in $(root_list); do echo $$p; done

root-list-files :
	@for p in $(root_list_files); do echo $$p; done


endif # xwmake/rules/root.mk
