ifndef	xwmake/rules/extract.mk
	xwmake/rules/extract.mk = included


ifneq ($(PKG_FETCH_METHOD),DONE)
$(EXTRACT_STAMP): $(ORIG_SRC_DIR)/.dir
endif

$(EXTRACT_STAMP): $(CHECK_STAMP) $(ST_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNEXTRACT_STAMP)
ifndef PKG_HAS_NO_SOURCES
  ifneq ($(filter CVS SVN,$(PKG_FETCH_METHOD)),) # DONE off
    ifdef PKG_NO_LINK
	@echo "$(I)SymLinking      $(PKG_BASE) $(PKG_VERSION) extracted sources  ... "
	@$(RM_R)                      $(PKG_ORIG_SRC_DIR) $(LOG)
	@$(LN_S) $(DL_DIR)/$(PKG_DIR) $(PKG_ORIG_SRC_DIR) $(LOG)
    else
	@echo "$(I)Extracting      $(PKG_BASE) $(PKG_VERSION) ... "
	@$(LN_CNT) make $(DL_DIR)/$(PKG_DIR) $(PKG_LN_CNT_FILE)
	@$(RM_R)                             $(PKG_ORIG_SRC_DIR)
	@$(LNTREE)      $(DL_DIR)/$(PKG_DIR) $(PKG_ORIG_SRC_DIR)
    endif
  else ifeq ($(PKG_FETCH_METHOD),NGET)
	@$(MKDIR_P) $(PKG_ORIG_SRC_DIR)
	@set -e; found=`cat $(FOUND_FILE_MAIN)`;		\
	echo  "$(I)Extracting      $$found ... ";		\
	case $$found in						\
	  *.tar*|*.tgz|*.tbz*|*.txz)				\
	    $(ZCAT) $(DL_DIR)/$$found |				\
	      $(TAR) -C $(PKG_ORIG_SRC_DIR) -xp			\
	        $(if $(EXTRACT_STRIP_NUM),--strip $(EXTRACT_STRIP_NUM))	\
	        -- $(EXTRACT_FILES)			||	\
		{ $(RM_R) $(PKG_ORIG_SRC_DIR); exit 1; }	\
	    ;;							\
	  *.zip)						\
	    cd $(ORIG_SRC_DIR) && unzip -q $(DL_DIR)/$$found	\
	    || { $(RM_R) $(PKG_ORIG_SRC_DIR); exit 1; }		\
	    ;;							\
	  *)							\
	    $(ZCAT) $(DL_DIR)/$$found >				\
	    $(PKG_ORIG_SRC_DIR)/$(EXTRACT_FILES)	||	\
		{ $(RM_R) $(PKG_ORIG_SRC_DIR); exit 1; }	\
	    ;;							\
	esac
  endif # NGET
endif	# ndef PKG_HAS_NO_SOURCES
ifdef POST_EXTRACT_CMD
	@echo "$(I)Post-Extracting $(PKG_BASE) $(PKG_VERSION) ... "
	@$(POST_EXTRACT_CMD)
endif
ifndef PKG_HAS_NO_SOURCES
  ifndef PKG_NO_LINK
    ifneq ($(PKG_FETCH_METHOD),DONE)
	@$(LN_CNT) make $(PKG_ORIG_SRC_DIR) $(PKG_LN_CNT_FILE)
	@echo "$(I)Changing to R/O $(PKG_BASE) $(PKG_VERSION) ... "
	@chmod -R a-w   $(PKG_ORIG_SRC_DIR) $(LOG)
    endif
  endif # ndef PKG_NO_LINK
endif
	@$(TOUCH) $@
endif # XDEP


unextract extract-clean: $(UNEXTRACT_STAMP)
$(UNEXTRACT_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo "$(I)Unextract       $(PKG) ... "
	@$(RM) $(EXTRACT_STAMP)
ifndef PKG_HAS_NO_SOURCES
  ifndef PKG_NO_LINK
	@$(CHMODALL) u+w d $(PKG_ORIG_SRC_DIR)
  endif # ndef PKG_NO_LINK
  ifneq ($(PKG_FETCH_METHOD),DONE)
	@$(RM_R)           $(PKG_ORIG_SRC_DIR)
  endif
ifndef PKG_NO_LINK
  ifneq ($(filter CVS SVN DONE,$(PKG_FETCH_METHOD)),)
	@if test -d             $(DL_DIR)/$(PKG_DIR); then \
	    if $(LN_CNT) check1 $(DL_DIR)/$(PKG_DIR) $(PKG_LN_CNT_FILE); then \
	       chmod -R u+w     $(DL_DIR)/$(PKG_DIR); \
	    fi; \
	fi
  endif
endif # ndef PKG_NO_LINK
endif # ndef PKG_HAS_NO_SOURCES
	@$(TOUCH) $@
endif # XDEP


endif # xwmake/rules/extract.mk
