ifndef	xwmake/rules/depend.mk
	xwmake/rules/depend.mk = included


$(DEPEND_STAMP): $(CONFIGURE_STAMP) $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNDEPEND_STAMP)
ifdef DEPEND_CMD
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo "$(I)Making Deps     $(PKG_BASE) $(PKG_VERSION) ... "
	@$(DEPEND_CMD)
endif
	@$(TOUCH) $@
endif # XDEP

undepend depend-clean: $(UNDEPEND_STAMP)

$(UNDEPEND_STAMP): $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(DEPEND_STAMP)
ifdef DEPCLEAN_CMD
	@echo "$(I)Cleaning deps   $(PKG_BASE) $(PKG_VERSION) ... "
	@$(DEPCLEAN_CMD)
endif
	@$(TOUCH) $@
endif # XDEP


endif # xwmake/rules/depend.mk
