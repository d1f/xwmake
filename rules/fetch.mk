ifndef	xwmake/rules/fetch.mk
	xwmake/rules/fetch.mk = included


.PHONY: fetch.main fetch.patch
fetch.main  : $(FETCH_MAIN_STAMP)
fetch.patch : $(FETCH_PATCH_STAMP)

fetch: fetch.main

ALL_FETCH_STAMPS = $(FETCH_STAMP) $(FETCH_MAIN_STAMP) $(FETCH_PATCH_STAMP)

# write this dependency in package makefile if patch file fetching needed
# (Debian sample):
#
# fetch: fetch.patch
# fetch.patch: PKG_FILE_BASE            = $(PKG_NAME)$(PKG_VER_PATCH)
# fetch.patch: PKG_SRC_FILE_SUFFIX      = .diff.gz
# fetch.patch: PKG_LOCAL__FILE_SUFFICES = .diff.gz
# fetch.patch: PKG_REMOTE_FILE_SUFFICES = .diff.gz

$(FETCH_STAMP) : $(FETCH_MAIN_STAMP)
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNFETCH_STAMP)
	@$(TOUCH) $@
endif # XDEP

$(ALL_FETCH_STAMPS) : $(DL_DIR)/.dir $(TMP_DIR)/.dir $(LOG_DIR)/.dir \
			$(ORIG_SRC_DIR)/.dir $(SRC_ST_DIR)/src-depot-list


ifdef PKG_NO_LINK
  define _DONE_FETCH_CMD_ # turned off with _*_
	@echo "$(I)SymLinking      $(PKG_BASE) $(PKG_VERSION) fetched sources  ... "
	$(RM_R)                 $(DL_DIR)/$(PKG_DIR) $(LOG)
	$(LN_S) $(PKG_ORIG_DIR) $(DL_DIR)/$(PKG_DIR) $(LOG)
  endef
else
  define _DONE_FETCH_CMD_ # turned off with _*_
	@echo "$(I)Linking         $(PKG_BASE) $(PKG_VERSION) fetched sources  ... "
	$(LN_CNT) make $(PKG_ORIG_DIR) $(PKG_LN_CNT_FILE)   $(LOG)
	$(RM_R)                        $(DL_DIR)/$(PKG_DIR) $(LOG)
	$(LNTREE)      $(PKG_ORIG_DIR) $(DL_DIR)/$(PKG_DIR) $(LOG)
  endef
endif

define CVS_FETCH_CMD
	@echo "$(I)CVS checkout    $(PKG_CVSDIR) $(pkg_CVSTAG) ..."
	@set -e;							\
	cd $(DL_DIR) &&							\
	cvs -d $(PKG_CVSROOT) co $(pkg_CVSTAG) $(pkg_CVSCODIR) $(PKG_CVSDIR) 2>&1
	@$(LN_CNT) make $(DL_DIR)/$(PKG_DIR) $(PKG_LN_CNT_FILE)
endef

define SVN_FETCH_CMD
	@echo "$(I)SVN checkout    $(PKG_SVNROOT) ..."
	@set -e;							\
	cd $(DL_DIR) &&							\
	$(SVN) co $(pkg_SVNTAG) $(PKG_SVNROOT) $(PKG_SVNCODIR) 2>&1
	@$(LN_CNT) make $(DL_DIR)/$(PKG_DIR) $(PKG_LN_CNT_FILE)
endef


#export NGET = wget
 export NGET = curl

define NGET_FETCH_CMD
	@echo "$(I)Fetching        $(PKG_BASE) $(PKG_VERSION) ($(FETCH_TYPE)) ..."
	@set -e;							\
	found=`$(XWMAKE_SCRIPT_DIR)/find-local-file			\
		$(SRC_ST_DIR)/src-depot-list				\
		$(PKG_FILE_BASE)					\
		$(PKG_LOCAL__FILE_SUFFICES)` || exit 1;			\
	if test -n "$$found"; then					\
	   echo "$(I1)found $$found";					\
	   cd $(DL_DIR) && $(LN_S) -f $$found	$(LOG)		&&	\
	   basename $$found > $(FOUND_FILE)			&&	\
	   $(TOUCH)           $(FOUND_FILE);				\
	else								\
	   if ! test -e $(BST_DIR)/myfile.install; then			\
		echo "$(I1)Installing      myfile ...";			\
		$(DOBLDMAKE) -f $(XWMAKE_PKG_DIR)/myfile/myfile.make	\
			PKG=myfile  INDENTS=$(INDENTS2) || exit 1;	\
		echo "$(I1)myfile done";				\
	   fi;								\
									\
	   if test -z "$$(which $(NGET) 2>/dev/null)"; then		\
		echo "$(I1)Installing $(NGET) ...";			\
		$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/$(NGET)/$(NGET).make	\
			PKG=$(NGET) INDENTS=$(INDENTS2) || exit 1;	\
		echo "$(I1)$(NGET) done";				\
	   fi;								\
									\
	   if test -n "$(strip $(PKG_SITES))"; then			\
	      echo "$(I1)Downloading one of multiple $(PKG_BASE) $(PKG_VERSION) ($(FETCH_TYPE)) ... "; \
	      $(RM) $(TMP_DIR)/$(PKG_FILE_BASE)*;			\
	      found=`$(XWMAKE_SCRIPT_DIR)/mget $(INDENTS2) $(TMP_DIR)	\
	      $(addsuffix /$(PKG_FILE_BASE),$(SRCARCHIVE_SITE_FIRST))	\
	      $(addsuffix /$(PKG_SITE_PATH)/$(PKG_FILE_BASE),		\
		             $(PKG_SITES))				\
	      $(addsuffix /$(PKG_FILE_BASE),$(SRCARCHIVE_SITE_LAST))	\
	      -- $(PKG_REMOTE_FILE_SUFFICES)`	|| exit 1;		\
	      bnfound=`basename $$found`	|| exit 1;		\
	      if test -n "$$found"; then				\
		 echo  $$bnfound > $(FOUND_FILE)		&&	\
		 $(TOUCH)          $(FOUND_FILE);			\
	      else							\
		echo "$(I1)No package source file found";		\
		exit 1;							\
	      fi;							\
	      mv $(TMP_DIR)/$$bnfound $(DL_DIR);			\
	   elif test -n "$(PKG_URL)"; then				\
	      echo "$(I1)Downloading $(PKG_BASE) $(PKG_VERSION) ($(FETCH_TYPE)) ... ";	\
	      $(RM) $(TMP_DIR)/$(PKG_FILE_BASE)*;			\
	      $(XWMAKE_SCRIPT_DIR)/$(NGET) $(TMP_DIR) $(PKG_URL)	\
					$(PKG_SRC_FILE) || exit 1;	\
	      echo "$(PKG_SRC_FILE)" > $(FOUND_FILE)		&&	\
	      $(TOUCH)                 $(FOUND_FILE);			\
	      mv $(TMP_DIR)/$(PKG_SRC_FILE) $(DL_DIR);			\
	   else								\
	      echo "$(I)PKG_SITES | PKG_URL does not supplied";		\
	      exit 1;							\
	   fi;								\
	fi
endef # NGET_FETCH_CMD

FETCH_CMD = $(if $(strip $(PKG_HAS_NO_SOURCES)),,\
$(if $(filter DONE,$(PKG_FETCH_METHOD)),$(DONE_FETCH_CMD),\
$(if $(filter NGET,$(PKG_FETCH_METHOD)),$(NGET_FETCH_CMD),\
$(if $(filter  CVS,$(PKG_FETCH_METHOD)), $(CVS_FETCH_CMD),\
$(if $(filter  SVN,$(PKG_FETCH_METHOD)), $(SVN_FETCH_CMD),\
$(error Unknown PKG_FETCH_METHOD: $(PKG_FETCH_METHOD)) )))))

$(FETCH_MAIN_STAMP) :
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNFETCH_MAIN_STAMP)
	@$(FETCH_CMD)
	@$(TOUCH) $@
endif # XDEP

$(FETCH_PATCH_STAMP) :
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNFETCH_PATCH_STAMP)
	@$(FETCH_CMD)
	@$(TOUCH) $@
endif # XDEP


.PHONY:  unfetch.%
unfetch: unfetch.patch unfetch.main $(UNFETCH_STAMP)
$(UNFETCH_STAMP):
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(FETCH_STAMP) $(SRC_ST_DIR)/$(PKG).found $(SRC_ST_DIR)/src-depot-list
	@$(TOUCH) $@
endif # XDEP

fetch-clean: unfetch

unfetch.main: unextract $(UNFETCH_MAIN_STAMP)
$(UNFETCH_MAIN_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo -n "$(I)Unfetch         $(PKG) (main) ... "
	@$(RM) $(FETCH_MAIN_STAMP)			\
		$(FOUND_FILE_MAIN)			\
		$(SRC_ST_DIR)/src-depot-list
ifndef PKG_HAS_NO_SOURCES
  ifneq ($(filter CVS SVN,$(PKG_FETCH_METHOD)),) # FIXME: DONE removed
	@-$(RM_R) $(DL_DIR)/$(PKG_DIR)
  endif
  ifeq ($(PKG_FETCH_METHOD),DONE)
	@if $(LN_CNT) check1 $(PKG_ORIG_DIR) $(PKG_LN_CNT_FILE); then \
	    chmod -f -R u+w  $(PKG_ORIG_DIR); true; \
	fi
  endif

  ifeq ($(PKG_FETCH_METHOD),NGET)
	@-$(RM) $(DL_DIR)/$(PKG_FILE_BASE)*
  endif
endif
	@$(TOUCH) $@
	@echo 'done'
endif # XDEP

unfetch.patch: $(UNFETCH_PATCH_STAMP)
$(UNFETCH_PATCH_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo -n "$(I)Unfetch         $(PKG) (patch) ... "
	@$(RM) $(FETCH_PATCH_STAMP)			\
		$(FOUND_FILE_PATCH)			\
		$(SRC_ST_DIR)/src-depot-list
	@echo 'done'
	@$(TOUCH) $@
endif # XDEP


endif # xwmake/rules/fetch.mk
