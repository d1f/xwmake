ifndef	xwmake/rules/update.mk
	xwmake/rules/update.mk = included


ifeq ($(PKG_FETCH_METHOD),NGET)
update: _unfetch
endif

ifneq ($(filter CVS SVN,$(PKG_FETCH_METHOD)),)
update: fetch
endif

ifeq ($(PKG_FETCH_METHOD),DONE)
update: unfetch
endif

update:
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
ifdef PKG_UPDATEABLE
	@echo "$(I)Updating        $(PKG) ... "
	@$(RM) $(ST_DIR)/$(PKG).*
  ifeq ($(PKG_FETCH_METHOD),CVS)
	@cd $(DL_DIR)/$(PKG_DIR) &&	\
	cvs -d $(PKG_CVSROOT) up -dPC $(pkg_CVSTAG) . 2>&1
	@$(TOUCH) $(FETCH_STAMP)
  endif
  ifeq ($(PKG_FETCH_METHOD),SVN)
	@cd $(DL_DIR)/$(PKG_DIR) &&	\
	$(SVN) update $(pkg_SVNTAG) . 2>&1
	@$(TOUCH) $(FETCH_STAMP)
  endif
  ifeq ($(PKG_FETCH_METHOD),DONE)
    ifneq ($(strip $(wildcard $(PKG_PATCHED_SRC_DIR)/CVS/*)),)
	@cd $(PKG_ORIG_DIR) &&	\
	cvs up -dPC . 2>&1
    endif
    ifneq ($(strip $(wildcard $(PKG_PATCHED_SRC_DIR)/.svn/*)),)
	@cd $(PKG_ORIG_DIR) &&	\
	svn update . 2>&1
    endif
  endif
  ifeq ($(PKG_FETCH_METHOD),NGET)
	#@$(RM) $(DEPOT_DIR)/$(PKG_FILE_BASE)*
  endif
endif
endif # XDEP


endif # xwmake/rules/update.mk
