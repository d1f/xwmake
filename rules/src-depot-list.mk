ifndef	xwmake/rules/src-depot-list.mk
	xwmake/rules/src-depot-list.mk = included


.PHONY: src-depot-list
src-depot-list: $(SRC_ST_DIR)/src-depot-list

$(SRC_ST_DIR)/src-depot-list: $(SRC_ST_DIR)/.dir
	$(TRACE_TARGET)
	@echo -n "$(I)Creating local sources list ... "
ifneq ($(strip $(DEPOT_DIR)),)
	@$(XWMAKE_SCRIPT_DIR)/gen-file-list $(DEPOT_DIR) > $@
endif
	@$(TOUCH) $@
	@echo 'done'


src-depot-list-clean:
	$(TRACE_TARGET)
	@echo -n "$(I)Remove local sources list ... "
	@$(RM) $(SRC_ST_DIR)/src-depot-list $(SRC_ST_DIR)/*.found
	@echo 'done'


endif # xwmake/rules/src-depot-list.mk
