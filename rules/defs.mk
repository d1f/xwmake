# defines that must be before rules and after defs

ifndef	xwmake/rules/defs.mk
	xwmake/rules/defs.mk = included


ALL_PATCHES  = $(strip $(PKG_PATCH_FILES)$(PKG_PATCH2_FILES)$(PKG_PATCH3_FILES)$(PATCH_CMD))

ifeq ($(ALL_PATCHES),)
  pkg_has_no_patches = defined
endif

ifdef SEPARATE_SRC_BLD_DIRS
  ifdef pkg_has_no_patches
    # .. lang=ru
    # �� ������ ��������� �� ����� fetch, extract, patch, link
    PKG_NO_LINK  = defined
    # FIXME: rename to PKG_DO_NOT_LINK
  endif
endif


endif # xwmake/rules/defs.mk
