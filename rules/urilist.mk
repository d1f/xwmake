ifndef	xwmake/rules/urilist.mk
	xwmake/rules/urilist.mk = included


.PHONY: urilist $(ST_DIR)/urilist
urilist: $(ST_DIR)/urilist


ifdef PKG_CVSROOT
 ifneq ($(strip $(PKG_CVSROOT)),$(strip $(XAP2_CVSROOT)))
  uri_list_item=cvs://$(PKG_CVSROOT)/$(PKG_CVSDIR)
 endif
endif
ifdef PKG_SVNROOT
  uri_list_item=$(PKG_SVNROOT)
endif
ifdef PKG_SITES
  uri_list_item=$(word $(words $(PKG_SITES)),$(PKG_SITES))/$(PKG_SITE_PATH)
endif
ifdef PKG_URL
  uri_list_item=$(PKG_URL)
endif

# <A HREF=PKG_DOWNLOAD_SITE_FULL_URL>PKG_NAME</A>
$(ST_DIR)/urilist: $(ST_DIR)/.dir
	$(TRACE_TARGET)
	@echo -n "$(I)Appending URI list for $(PKG)... "
ifneq ($(strip $(uri_list_item)),)
	@echo '<a href=$(uri_list_item)>$(PKG) $(PKG_VERSION)$(PKG_VER_PATCH)</a><br>' | sed -e 's,\([^:]\)//,\1/,g' >> $@
endif
	@echo 'done'

urilist-clean uriclean:
	$(TRACE_TARGET)
	@echo -n "$(I)Remove URI list ... "
	@$(RM) $(ST_DIR)/urilist
	@echo 'done'


endif # xwmake/rules/urilist.mk
