ifndef	xwmake/rules/link.mk
	xwmake/rules/link.mk = included


$(LINK_STAMP): $(PATCH_STAMP) $(ST_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNLINK_STAMP)
ifndef PKG_HAS_NO_SOURCES
  ifdef SEPARATE_SRC_BLD_DIRS
	@$(APPEND_LOG_SEPARATOR_CMD)
	@$(RM_R)			$(PKG_BLD_DIR) $(LOG)
	@$(MKDIR_P)			$(PKG_BLD_DIR) $(LOG)
  else
	@echo "$(I)Linking         $(PKG_BASE) $(PKG_VERSION) sources for building ... "
	@$(APPEND_LOG_SEPARATOR_CMD)
	@$(RM_R)				$(PKG_BLD_DIR) $(LOG)
	@$(LNTREE) $(PKG_PATCHED_SRC_DIR)	$(PKG_BLD_DIR) $(LOG)
  endif
endif
	@$(TOUCH) $@
endif # XDEP


unlink link-clean: $(UNLINK_STAMP)

$(UNLINK_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM)       $(LINK_STAMP)	\
	    $(PRE_CONFIGURE_STAMP)	\
		$(CONFIGURE_STAMP)	\
		   $(DEPEND_STAMP)	\
		    $(BUILD_STAMP)
	@echo "$(I)Unlinking       $(PKG_BASE) $(PKG_VERSION) sources ... "
	@$(CHMODALL) u+w d	$(PKG_BLD_DIR) $(LOG)
	@$(RM_R)		$(PKG_BLD_DIR) $(LOG)
	@$(TOUCH) $@
endif # XDEP


endif # xwmake/rules/link.mk
