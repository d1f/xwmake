ifndef	xwmake/rules/nested.mk
	xwmake/rules/nested.mk = included


# .. lang=ru
# ������ ��������� �������.
# �����, ������� �������� ������ ��������� �������,
# ��� ������ ���� ������� �� ����� �� ���� �� ���� ������� ������.
# ������������, � ��������, � :term:`����������� <���������>`.
NESTED_PKGES ?=

ifdef NESTED_PKGES

  src_nested_f=$(addprefix   $(SRC_ST_DIR)/,$(addsuffix .$(1), $(NESTED_PKGES)))
patch_nested_f=$(addprefix $(PATCH_ST_DIR)/,$(addsuffix .$(1), $(NESTED_PKGES)))
      nested_f=$(addprefix       $(ST_DIR)/,$(addsuffix .$(1), $(NESTED_PKGES)))


       fetch : $(call src_nested_f,fetch)
     unfetch : $(call src_nested_f,unfetch)
     refetch : $(call src_nested_f,refetch)
      update : $(call src_nested_f,update)

       check : $(call src_nested_f,check)
     uncheck : $(call src_nested_f,uncheck)
     recheck : $(call src_nested_f,recheck)

  save-files : $(call src_nested_f,save-files)

     extract : $(call src_nested_f,extract)
   unextract : $(call src_nested_f,unextract)
   reextract : $(call src_nested_f,reextract)

       patch : $(call patch_nested_f,patch)
     unpatch : $(call patch_nested_f,unpatch)
     repatch : $(call patch_nested_f,repatch)

        link : $(call nested_f,link)
      unlink : $(call nested_f,unlink)
      relink : $(call nested_f,relink)

   configure : $(call nested_f,configure)
pre-configure: $(call nested_f,pre-configure)
 unconfigure : $(call nested_f,unconfigure)
 reconfigure : $(call nested_f,reconfigure)

      depend : $(call nested_f,depend)
    undepend : $(call nested_f,undepend)
    redepend : $(call nested_f,redepend)
         dep : $(call nested_f,dep)

       build : $(call nested_f,build)
     unbuild : $(call nested_f,unbuild)
     rebuild : $(call nested_f,rebuild)

     install : $(call nested_f,install)
   uninstall : $(call nested_f,uninstall)
   reinstall : $(call nested_f,reinstall)

  install-root : $(call nested_f,install-root)
reinstall-root : $(call nested_f,reinstall-root)

list-install : $(call nested_f,list-install)
  rsize      : $(call nested_f,rsize)
  rsize-sum  : $(call nested_f,rsize-sum)

        pack : $(call nested_f,pack)
      unpack : $(call nested_f,unpack)
      repack : $(call nested_f,repack)

  pack-status : $(call nested_f,pack-status)
unpack-status : $(call nested_f,unpack-status)
repack-status : $(call nested_f,repack-status)

    logclean : $(call nested_f,logclean)
       unlog : $(call nested_f,unlog)
     urilist : $(call nested_f,urilist)

endif


endif # xwmake/rules/nested.mk
