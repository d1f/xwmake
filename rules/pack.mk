ifndef	xwmake/rules/pack.mk
	xwmake/rules/pack.mk = included


.PHONY: pack

PACK_INST_DIR   =      $(INST_DIR)/pack
 PKG_PACK_DIR   = $(PACK_INST_DIR)/$(PKG)

pack_pkgver = $(if $(PKG_VERSION),$(PKG_VERSION),0)
PACK_NAME     = $(subst _,-,$(PKG_BASE))
PKG_PACK_FILE = $(PACK_NAME)_$(pack_pkgver)_$(ARCH).opk

define _control_file_
	Description: Too long to describe
	Section: system/applications
	Priority: optional:
	Maintainer: d1f <dmitry@sigrand.com>"
	Architecture: all
	Homepage: http://sigrand.ru/
	Source:
	Depends:
endef

define ECHO_CONTROL_FILE
  { \
	echo "Package: $(PACK_NAME)"	; \
	echo "Version: $(pack_pkgver)"	; \
	echo "Architecture: $(ARCH)"	; \
  }
endef

define ECHO_STATUS_FILE
  { \
	echo "Package: $(subst _,-,$(PKG_BASE))"; \
	echo "Version: $(pack_pkgver)"		; \
	echo "Provides:"			; \
	echo "Status: install ok installed"	; \
	echo "Architecture: $(ARCH)"		; \
	echo "Installed-Time: `date +%s`"	; \
	echo ""					; \
  }
endef


$(PACK_STAMP): $(call bstamp_f,install,dpkg)
$(PACK_STAMP): $(INSTALL_STAMP)
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNPACK_STAMP)
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo    "$(I)Packing         $(PKG_BASE) ... "
	@test -f $(IWL_DIR)/$(PKG).iwl || { echo "$(I)No $(PKG).iwl exist"; false; }

	@$(RM_R)    $(PKG_PACK_DIR)
	@$(MKDIR_P) $(PKG_PACK_DIR)/DEBIAN

	@echo "$(I1)Copying data          ... "
	@$(LIST_INSTALL_FILES) | sed -e 's,$(ROOT_DIR)/,,g' | \
	$(TAR) -C $(ROOT_DIR) -T- -cf- | $(TAR) -C $(PKG_PACK_DIR) -xf-

	@echo "$(I1)Writing control file  ... "
	@$(ECHO_CONTROL_FILE) > $(PKG_PACK_DIR)/DEBIAN/control

	@#echo '#!/bin/sh' > $(PKG_PACK_DIR)/DEBIAN/preinst
	@#echo '#!/bin/sh' > $(PKG_PACK_DIR)/DEBIAN/postinst
	@#echo '#!/bin/sh' > $(PKG_PACK_DIR)/DEBIAN/prerm
	@#echo '#!/bin/sh' > $(PKG_PACK_DIR)/DEBIAN/postrm
	@#chmod +x $(addprefix $(PKG_PACK_DIR)/DEBIAN/,preinst postinst prerm postrm)

	@echo "$(I1)Packing opkg          ... "
	@GZIP=-1 TAR_OPTIONS="-b1 --exclude-backups --exclude-vcs --exclude=.dir \
		--group=0 --owner=0" \
		$(BINST_DIR)/bin/dpkg-deb -b $(PKG_PACK_DIR) \
			$(PACK_INST_DIR)/$(PKG_PACK_FILE) $(LOG)
	@$(TOUCH) $@
	@echo "$(I)done"
endif # XDEP


unpack pack-clean: $(UNPACK_STAMP)

$(UNPACK_STAMP): $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo -n "$(I)Removing        $(PKG_BASE) pack ... "
	@$(RM)   $(PACK_STAMP)
	@$(RM)   $(PACK_INST_DIR)/$(PKG_PACK_FILE)
	@$(RM_R) $(PKG_PACK_DIR)
	@$(RM) $(PACK_STAMP)
	@$(TOUCH) $@
	@echo done
endif # XDEP

repack : unpack pack



ROPKG_DIR = $(ROOT_DIR)/var/opkg

.PHONY : %pack-status
$(PACK_STATUS_STAMP) : $(INSTALL_STAMP)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNPACK_STATUS_STAMP)
	@echo "$(I1)Append  $(PACK_NAME) status file    ... "
	@$(MKDIR_P)             $(ROPKG_DIR)
	@$(ECHO_STATUS_FILE) >> $(ROPKG_DIR)/status

	@echo "$(I1)Writing $(PACK_NAME) control file  ... "
	@$(MKDIR_P)             $(ROPKG_DIR)/info
	@$(ECHO_CONTROL_FILE) > $(ROPKG_DIR)/info/$(PACK_NAME).control

	@echo "$(I1)Writing $(PACK_NAME) list file     ... "
	@$(LIST_INSTALL_FILES) | sed -e 's,$(ROOT_DIR),,g' > \
		$(ROPKG_DIR)/info/$(PACK_NAME).list
	@$(TOUCH) $@
endif # XDEP


unpack-status : $(UNPACK_STATUS_STAMP)

$(UNPACK_STATUS_STAMP):
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo "$(I)Removing        $(PACK_NAME) status files    ... "
	@$(RM) $(PACK_STATUS_STAMP)
	@$(RM)	  $(ROPKG_DIR)/info/$(PACK_NAME).*
	@$(MKDIR_P) $(ROPKG_DIR)
	@if test -f $(ROPKG_DIR)/status; then  \
	    $(XWMAKE_SCRIPT_DIR)/opkg-status-cut \
		  $(ROPKG_DIR)/status $(PACK_NAME) \
		> $(ROPKG_DIR)/status.new && \
	    mv	  $(ROPKG_DIR)/status.new \
		  $(ROPKG_DIR)/status; \
	fi
	@if ! test -s $(ROPKG_DIR)/status; then $(RM_R) $(ROPKG_DIR); fi
	@$(TOUCH) $@
endif # XDEP

repack-status : unpack-status pack-status


endif # xwmake/rules/pack.mk
