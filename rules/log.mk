ifndef	xwmake/rules/log.mk
	xwmake/rules/log.mk = included


log-clean logclean unlog :
	$(TRACE_TARGET)
	@echo -n "$(I)Remove          $(PKG) log files ... "
	@$(RM) $(LOG_DIR)/$(PKG).*.log? $(LOG_DIR)/$(PKG).*.log
	@echo 'done'


# FIXME: normalize targets
.PHONY: %-log
%-log :
	@cat $(LOG_DIR)/$(PKG).$*.log


endif # xwmake/rules/log.mk
