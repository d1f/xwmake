# inter-platform rules for the same package:

ifndef	xwmake/rules/inter-platform.mk
	xwmake/rules/inter-platform.mk = included


ifdef XDEP
	make_pkg = $(XDEP_TARGET)
else  # XDEP

define make_platform
$(MAKE) -f $(call pkg_file_f,$(PKG)) \
	$(stfile_target) PLATFORM=$* INDENTS=$(INDENTS)i
endef

trace_platform     = @echo \
"$(I)Making          $(PKG), target: $(stfile_target), PLATFORM: $*"
trace_platform_end = @echo \
"$(I)made            $(PKG), target: $(stfile_target), PLATFORM: $*"
#, LABEL: $(LABEL)
#, LABEL: $(LABEL)

endif # XDEP

$(_ST_DIR)/%/$(PKG).patch:
	$(trace_platform)
	@$(make_platform)
	$(trace_platform_end)

$(_ST_DIR)/%/$(PKG).unpatch:
	$(trace_platform)
	@$(make_platform)
	$(trace_platform_end)

unextract: $(foreach P,$(ALL_PLATFORMS),$(_ST_DIR)/$(P)/$(PKG).unpatch)


endif # xwmake/rules/inter-platform.mk
