#FIXME: recursion call of build target of the same package!
#ifndef	xwmake/rules/build.mk
	xwmake/rules/build.mk = included


$(BUILD_STAMP): $(DEPEND_STAMP) $(ROOT_DIR)/.dir $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNBUILD_STAMP)
ifdef BUILD_CMD
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo "$(I)Building        $(PKG_BASE) $(PKG_VERSION) ... "
	@$(BUILD_CMD)
endif
	@$(TOUCH) $@
endif # XDEP


.PHONY: unbuild
build-clean clean unbuild: uninstall $(UNBUILD_STAMP) $(LOG_DIR)/.dir

$(UNBUILD_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(BUILD_STAMP)
ifdef CLEAN_CMD
	@echo "$(I)Cleaning        $(PKG_BASE) $(PKG_VERSION) ... "
	@-$(CLEAN_CMD)
endif
	@$(TOUCH) $@
endif # XDEP


#endif # xwmake/rules/build.mk
