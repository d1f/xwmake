ifndef	xwmake/rules/host.mk
	xwmake/rules/host.mk = included


# normalized arch
narch_cmd=sed -e s/i.86/i386/ -e s/sun4u/sparc64/ -e s/arm.*/arm/ \
              -e s/sa110/arm/ -e s/s390x/s390/ -e s/parisc64/parisc/ \
              -e s/ppc.*/powerpc/ -e s/mips.*/mips/ -e s/sh.*/sh/


ifeq ($(filter var-clean varclean,$(MAKECMDGOALS)),)

$(_ST_DIR)/build-machine.mk: $(_ST_DIR)/.dir
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo -n "$(I)Guessing build machine triplet ... "
	@set -e;	\
	 build=`$(XWMAKE_SCRIPT_DIR)/config.guess` || exit 1;	\
	nbuild=`echo $$build | $(narch_cmd)`       || exit 1;	\
		echo " BUILD =  $$build"  > $@;			\
		echo " BUILD_ARCH = `echo  $$build | cut -d- -f1`" >> $@; \
		echo ""                  >> $@;			\
		echo "NBUILD = $$nbuild" >> $@;			\
		echo "NBUILD_ARCH = `echo $$nbuild | cut -d- -f1`" >> $@
	@echo done
endif # XDEP

$(_ST_DIR)/hostname.mk: $(_ST_DIR)/.dir
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@echo -n "$(I)Guessing host name ... "
	@host=`hostname -f 2>/dev/null || hostname 2>/dev/null || echo unknown`; \
		echo "HOSTNAME_F   = $$host" > $@
	@echo done
endif # XDEP

endif


endif # xwmake/rules/host.mk
