# cross package dependency generation rules:
ifndef	xwmake/rules/xdep.mk
	xwmake/rules/xdep.mk = included


.PHONY: xdep% unxdep%

xdep-clean xdepclean unxdep:
	@echo -n "$(I)Removing $(notdir $(PKG_XDEP)) ... "
	@$(RM) $(PKG_XDEP)
	@echo done

xdeps-clean xdepsclean unxdeps:
	@echo -n "$(I)Removing all of xdeps ... "
	@$(RM_R) $(XDEP_DIR)
	@echo done

xdep: $(PKG_XDEP)
$(XDEP_DIR)/%.xdep : $(call pkg_file_f,$*) $(XDEP_DIR)/.dir
	@echo -n "$(I)Generating $*.xdep ... "
	@$(RM) $@
	@$(DOTGTMAKE) -f $(call pkg_file_f,$*) install-root PKG=$* XDEP=1
	@echo "$(I)done"


ifdef XDEP
.PHONY: $(FETCH_STAMP) $(FETCH_MAIN_STAMP) $(FETCH_PATCH_STAMP)
.PHONY: $(CHECK_STAMP) $(EXTRACT_STAMP) $(PATCH_STAMP) $(LINK_STAMP)
.PHONY: $(PRE_CONFIGURE_STAMP) $(CONFIGURE_STAMP) $(DEPEND_STAMP)
.PHONY: $(BUILD_STAMP) $(INSTALL_STAMP) $(INSTALL_ROOT_STAMP)
endif


#ifndef XDEP
# ifeq ($(filter var-clean varclean clean-dl-links,$(MAKECMDGOALS)),)
#  -include $(addprefix $(XDEP_DIR)/,$(addsuffix .xdep,$(ALL_PKGES)))
# endif
#endif


endif # xwmake/rules/xdep.mk
