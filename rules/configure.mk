ifndef	xwmake/rules/configure.mk
	xwmake/rules/configure.mk = included


.PHONY: configure-help configure-help-recursive
configure-help : $(LINK_STAMP)
	@ $(or	$(wildcard $(PKG_BLD_SUB_DIR)/configure),\
		$(wildcard $(PKG_SRC_SUB_DIR)/configure),\
		$(error No configure script found!)) --help

configure-help-recursive : $(LINK_STAMP)
	@ $(or	$(wildcard $(PKG_BLD_SUB_DIR)/configure),\
		$(wildcard $(PKG_SRC_SUB_DIR)/configure),\
		$(error No configure script found!)) --help=recursive

$(PRE_CONFIGURE_STAMP): $(LINK_STAMP) $(PKG_BLD_DIR)/.dir $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
ifdef PRE_CONFIGURE_CMD
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo "$(I)Pre-Configuring $(PKG_BASE) $(PKG_VERSION) ... "
	@$(PRE_CONFIGURE_CMD)
endif
	@$(TOUCH) $@
endif # XDEP

$(CONFIGURE_STAMP): $(PRE_CONFIGURE_STAMP) $(PKG_BLD_DIR)/.dir $(PKG_BLD_SUB_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
ifdef CONFIGURE_CMD
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo "$(I)Configuring     $(PKG_BASE) $(PKG_VERSION) ... "
	@$(CONFIGURE_CMD)
endif
	@$(TOUCH) $@
endif # XDEP


ifndef SEPARATE_SRC_BLD_DIRS
  distclean configure-clean: depend-clean
endif

distclean configure-clean unconfigure: $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(PRE_CONFIGURE_STAMP) $(CONFIGURE_STAMP) $(BUILD_STAMP)
ifdef DISTCLEAN_CMD
	@echo "$(I)DistCleaning    $(PKG) ... "
	@-$(DISTCLEAN_CMD)
endif
endif # XDEP


endif # xwmake/rules/configure.mk
