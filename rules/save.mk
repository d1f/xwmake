ifndef	xwmake/rules/save.mk
	xwmake/rules/save.mk = included


fetched_links_cmd = $(FIND)  $(DL_DIR) -maxdepth 1 -type l
fetched_dirs__cmd = $(FIND)  $(DL_DIR) -maxdepth 1 -type d | egrep -v '$(DL_DIR)$$'
var_dirs_nodl_cmd = $(FIND) $(VAR_DIR) -maxdepth 1 -type d | egrep -v '$(DL_DIR)$$' | egrep -v '$(VAR_DIR)$$'

.PHONY: var-clean varclean
var-clean varclean: clean-dl-links
	@dirs=`$(var_dirs_nodl_cmd)`; \
	if test -n "$$dirs" ; then \
		for i in $$dirs $(VAR_DIR); do echo "$(I)Removing $$i"; \
			$(CHMODALL) u+w d $$i; \
			$(RM_R) $$i; \
		done; \
	fi

.PHONY: clean-dl-links
clean-dl-links:
	@if test -d $(DL_DIR); then \
		links=`$(fetched_links_cmd)` && \
		if test -n "$$links"; then $(RM) -v $$links; fi; \
	fi

.PHONY: save-all-files
save-all-files:
	@$(RM) $(SRC_ST_DIR)/src-depot-list
	@echo "$(I)Moving downloaded files to sources depot $(DEPOT_DIR) ... "
	@cd $(DL_DIR) && files=`$(FIND) . -maxdepth 1 -type f ! -name .dir` && \
	if test -n "$$files"; then \
	   for i in $$files; do \
		b=`basename $$i`     && \
		echo "$(I1)$$b"      && \
		mv $$b  $(DEPOT_DIR) && \
		$(LN_S) $(DEPOT_DIR)/$$b >/dev/null || exit 1; \
	   done; \
	fi
	@echo "$(I)done"

.PHONY: save-files
save-files:
	@$(RM) $(SRC_ST_DIR)/src-depot-list
	@echo "$(I)Moving downloaded files to sources depot $(DEPOT_DIR) ... "
	@cd $(DL_DIR) && files=`$(FIND) . -maxdepth 1 -type f	\
				-name $(PKG_FILE_BASE)\*` && \
	if test -n "$$files"; then \
	   for i in $$files; do \
		b=`basename $$i`     && \
		echo "$(I1)$$b"      && \
		mv $$b  $(DEPOT_DIR) && \
		$(LN_S) $(DEPOT_DIR)/$$b $(LOG) || exit 1; \
	   done; \
	fi
	@echo '$(I)done'

.PHONY: save-all-dirs
save-all-dirs:
	@$(RM) $(SRC_ST_DIR)/src-depot-list
	@echo "$(I)Moving checkouted dirs to sources repository $(DEPOT_DIR) ... "
	@dirs=`$(fetched_dirs__cmd)` && if test -n "$$dirs"; then \
		mv $$dirs $(DEPOT_DIR) || exit 1; fi


endif # xwmake/rules/save.mk
