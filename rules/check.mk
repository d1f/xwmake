ifndef	xwmake/rules/check.mk
	xwmake/rules/check.mk = included


check: $(CHECK_STAMP) fetch

$(CHECK_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
 ifndef PKG_HAS_NO_SOURCES
  ifeq ($(PKG_FETCH_METHOD),NGET)
	@if test -e    $(FOUND_FILE_MAIN); then \
	    found=`cat $(FOUND_FILE_MAIN)`;   \
	    case $$found in \
		*.gz)	if test -z "$$(which gunzip 2>/dev/null)"; then \
				echo "$(I)Installing gzip ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/gzip.make PKG=gzip || exit 1; \
				echo "$(I)gzip done"; \
			fi;; \
		*.bz2)	if test -z "$$(which bunzip2 2>/dev/null)"; then \
				echo "$(I)Installing bzip2 ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/bzip2.make PKG=bzip2 || exit 1; \
				echo "$(I)bzip2 done"; \
			fi;; \
		*.xz)	if test -z "$$(which unxz 2>/dev/null)"; then \
				echo "$(I)Installing    xz ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/xz.make    PKG=xz    || exit 1; \
				echo "$(I)xz done"; \
			fi;; \
		*.zip)	if test -z "$$(which unzip 2>/dev/null)"; then \
				echo "$(I)Installing unzip ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/unzip.make PKG=unzip || exit 1; \
				echo "$(I)unzip done"; \
			fi;; \
	    esac; \
	    echo -n "$(I)"; $(ZCHECK) \
		$(DL_DIR)/$$found; \
	fi
	@if test -e    $(FOUND_FILE_PATCH); then \
	    found=`cat $(FOUND_FILE_PATCH)`;     \
	    case $$found in \
		*.gz)	if test -z "$$(which gunzip 2>/dev/null)"; then \
				echo "$(I)Installing gzip ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/gzip.make PKG=gzip || exit 1; \
				echo "$(I)gzip done"; \
			fi;; \
		*.bz2)	if test -z "$$(which bunzip2 2>/dev/null)"; then \
				echo "$(I)Installing bzip2 ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/bzip2.make PKG=bzip2 || exit 1; \
				echo "$(I)bzip2 done"; \
			fi;; \
		*.xz)	if test -z "$$(which unxz 2>/dev/null)"; then \
				echo "$(I)Installing    xz ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/xz.make    PKG=xz    || exit 1; \
				echo "$(I)xz done"; \
			fi;; \
		*.zip)	if test -z "$$(which unzip 2>/dev/null)"; then \
				echo "$(I)Installing unzip ..."; \
				$(DOBLDMAKE) -f $(TOOLS_PKG_DIR)/unzip.make PKG=unzip || exit 1; \
				echo "$(I)unzip done"; \
			fi;; \
	    esac; \
	    echo -n "$(I)"; $(ZCHECK) \
		$(DL_DIR)/$$found; \
	fi
  endif
 endif
	@$(RM) $(UNCHECK_STAMP)
	@$(TOUCH) $@
endif # XDEP


uncheck: $(UNCHECK_STAMP)

$(UNCHECK_STAMP):
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(CHECK_STAMP)
	@$(TOUCH) $@
endif # XDEP


endif # xwmake/rules/check.mk
