# inter-package rules for the same platform:

ifndef	xwmake/rules/inter-package.mk
	xwmake/rules/inter-package.mk = included


ifdef XDEP
	make_pkg = $(XDEP_TARGET)
else  # XDEP

define make_pkg
$(MAKE) -f $(call pkg_file_f,$*) \
	$(stfile_target) PKG=$* INDENTS=$(INDENTS)i
endef

trace_pkg     = @echo "$(I)Making          $*, target: $(stfile_target), PLATFORM: $(PLATFORM)"
trace_pkg_end = @echo "$(I)made            $*, target: $(stfile_target), PLATFORM: $(PLATFORM)"
#, LABEL: $(LABEL)
#, LABEL: $(LABEL)

endif # XDEP


$(SRC_ST_DIR)/%.fetch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.unfetch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.refetch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.check:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.uncheck:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.recheck:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.save-files:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.update:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.extract:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.unextract:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(SRC_ST_DIR)/%.reextract:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(PATCH_ST_DIR)/%.patch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(PATCH_ST_DIR)/%.unpatch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(PATCH_ST_DIR)/%.repatch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.link:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.unlink:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.relink:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.pre-configure:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.configure:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.unconfigure:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.reconfigure:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.depend:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.undepend:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.redepend:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.build:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.unbuild:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.rebuild:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.clean:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.distclean:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.install:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.uninstall:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.reinstall:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.install-root:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.uninstall-root:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.reinstall-root:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.pack:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.unpack:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.repack:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.pack-status:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.unpack-status:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.repack-status:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.list-install:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.rsize:
	@echo "------- $*:"
	@$(make_pkg)

$(ST_DIR)/%.rsize-sum:
	@$(make_pkg)

$(ST_DIR)/%.logclean:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.unlog:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(ST_DIR)/%.urilist:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)


  $(SRC_ST_DIR)/%.fetch   :   $(SRC_ST_DIR)/%@.fetch
	@$(TOUCH) $@

  $(SRC_ST_DIR)/%.check   :   $(SRC_ST_DIR)/%@.check
	@$(TOUCH) $@

  $(SRC_ST_DIR)/%.extract :   $(SRC_ST_DIR)/%@.extract
	@$(TOUCH) $@

$(PATCH_ST_DIR)/%.patch   : $(PATCH_ST_DIR)/%@.patch
	@$(TOUCH) $@

      $(ST_DIR)/%.link    :       $(ST_DIR)/%@.link
	@$(TOUCH) $@

      $(ST_DIR)/%.pre-configure : $(ST_DIR)/%@.pre-configure
	@$(TOUCH) $@

      $(ST_DIR)/%.configure :     $(ST_DIR)/%@.configure
	@$(TOUCH) $@

      $(ST_DIR)/%.depend  :       $(ST_DIR)/%@.depend
	@$(TOUCH) $@

      $(ST_DIR)/%.build   :       $(ST_DIR)/%@.build
	@$(TOUCH) $@

      $(ST_DIR)/%.install :       $(ST_DIR)/%@.install
	@$(TOUCH) $@

      $(ST_DIR)/%.install-root :       $(ST_DIR)/%@.install-root
	@$(TOUCH) $@




# TODO: move to inter-platform section

$(_ST_DIR)/build/%.patch: PLATFORM=build
$(_ST_DIR)/build/%.patch:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(BST_DIR)/%.build: PLATFORM=build
$(BST_DIR)/%.build:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(BST_DIR)/%.install: PLATFORM=build
$(BST_DIR)/%.install:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(BST_DIR)/%.uninstall: PLATFORM=build
$(BST_DIR)/%.uninstall:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(BST_DIR)/%.install-root: PLATFORM=build
$(BST_DIR)/%.install-root:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)

$(BST_DIR)/%.uninstall-root: PLATFORM=build
$(BST_DIR)/%.uninstall-root:
	$(trace_pkg)
	@$(make_pkg)
	$(trace_pkg_end)


endif # xwmake/rules/inter-package.mk
