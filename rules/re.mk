# simple re* targets

ifndef	xwmake/rules/re.mk
	xwmake/rules/re.mk = included


    .PHONY: _uninstall reinstall
 reinstall: _uninstall   install
_uninstall:
	@$(RM) $(INSTALL_STAMP)

    .PHONY:      _uninstall-root reinstall-root
 reinstall-root: _uninstall-root   install-root
_uninstall-root:
	@$(RM) $(INSTALL_ROOT_STAMP)

  .PHONY: _unbuild rebuild
 rebuild: _unbuild   build
_unbuild: _uninstall
	@$(RM) $(BUILD_STAMP)

      .PHONY: _unconfigure reconfigure
 reconfigure: _unconfigure   configure
_unconfigure: _unbuild
	@$(RM) $(CONFIGURE_STAMP)

   .PHONY: _undepend redepend
 redepend: _undepend   depend
_undepend: _unconfigure
	@$(RM) $(DEPEND_STAMP)

.PHONY: relink
relink: unlink link

 .PHONY: repatch
repatch: unpatch unlog patch

   .PHONY: reextract
reextract: unextract extract


 .PHONY: _uncheck recheck
recheck: _uncheck   check

_uncheck:
	@$(RM) $(CHECK_STAMP)*


 .PHONY: _unfetch refetch
refetch: _unfetch   fetch

_unfetch:
	@$(RM) $(FETCH_STAMP)*


endif # xwmake/rules/re.mk
