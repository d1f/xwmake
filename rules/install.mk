ifndef	xwmake/rules/install.mk
	xwmake/rules/install.mk = included


$(INSTALL_STAMP):  $(IWL_DIR)/.dir $(INST_DIR)/.dir $(LOG_DIR)/.dir
$(INSTALL_STAMP): $(BUILD_STAMP)
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNINSTALL_STAMP)
ifdef INSTALL_CMD
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo "$(I)Installing      $(PKG_BASE) $(PKG_VERSION) ... "
	@$(INSTALL_CMD)
endif
ifdef POST_INSTALL_CMD
	@$(POST_INSTALL_CMD)
endif
	@$(TOUCH) $@
endif # XDEP


ifeq ($(filter any checkinstall installwatch strace coreutils-wrappers,$(strip $(PKG))),)

ifndef XDEP

ifneq ($(INSTALL_CMD)$(INSTALL_ROOT_CMD),)
 ifeq ($(IWTYPE),installwatch)
  $(INSTALL_STAMP) $(INSTALL_ROOT_STAMP): $(call bstamp_f,install,checkinstall)
 else ifeq ($(IWTYPE),strace)
  $(INSTALL_STAMP) $(INSTALL_ROOT_STAMP): $(call bstamp_f,install,strace)
 endif
endif

$(EXTRACT_STAMP): $(call bstamp_f,install,coreutils-wrappers)

endif

endif


uninstall: $(UNINSTALL_STAMP)

$(UNINSTALL_STAMP): $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(INSTALL_STAMP)
  ifdef    UNINSTALL_CMD
	@echo "$(I)UnInstalling    $(PKG_BASE) $(PKG_VERSION) ... "
	@$(UNINSTALL_CMD)
  endif
  ifdef	   UNINSTALL2_CMD
	@$(UNINSTALL2_CMD)
  endif
	@$(TOUCH) $@
endif # XDEP


.PHONY: install-root uninstall-root
  install-root :   $(INSTALL_ROOT_STAMP)
uninstall-root : $(UNINSTALL_ROOT_STAMP)

$(INSTALL_ROOT_STAMP):  $(IWL_DIR)/.dir $(ROOT_DIR)/.dir  $(LOG_DIR)/.dir $(INSTALL_STAMP)
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(UNINSTALL_ROOT_STAMP)
ifdef INSTALL_ROOT_CMD
	@$(APPEND_LOG_SEPARATOR_CMD)
	@echo "$(I)Installing root $(PKG_BASE) $(PKG_VERSION) ... "
	@$(INSTALL_ROOT_CMD)
endif
	@$(TOUCH) $@
endif # XDEP

$(UNINSTALL_ROOT_STAMP): $(LOG_DIR)/.dir
	$(TRACE_TARGET)
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(RM) $(INSTALL_ROOT_STAMP)
  ifdef    UNINSTALL_ROOT_CMD
	@echo "$(I)UnInstalling root    $(PKG_BASE) $(PKG_VERSION) ... "
	@$(UNINSTALL_ROOT_CMD)
  endif
	@$(TOUCH) $@
endif # XDEP


.PHONY: list-install list-inst
list-install list-inst:
ifdef XDEP
	$(XDEP_TARGET)
else  # XDEP
	@$(LIST_INSTALL)
endif # XDEP


endif # xwmake/rules/install.mk
