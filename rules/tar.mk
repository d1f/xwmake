# FIXME: correct directory handling
ifndef	xwmake/rules/tar.mk
	xwmake/rules/tar.mk = included


tarbase = $(notdir $(PRJ_DIR))

.PHONY: tar
tar :
	@BZIP2=-9; rev=`$(XWMAKE_SCRIPT_DIR)/gitrev`; \
	tarfile=$(tarbase)_$${rev}.tar.bz2; \
	echo -n  "$(I)Packing $$tarfile ... "; \
	$(TAR) -c -b1 --bzip2 -f $$tarfile \
		$(notdir $(PRJ_DIR) $(COMMON_DIR) $(TOOLS_DIR) $(XWMAKE_DIR)) \
		README*
	@echo done

tar-clean tarclean :
	@$(RM) -v $(tarbase)_*.tar.bz2


endif # xwmake/rules/tar.mk
