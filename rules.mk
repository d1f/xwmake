ifndef	xwmake/rules.mk
	xwmake/rules.mk = included


ifndef xwmake/defs.mk
include $(DEFS)
endif


include $(XWMAKE_DIR)/rules/defs.mk


# to avoid extra rules chain checking
Makefile:;
%.mk:;
%.make:;


.PHONY: post-% pre-% %clean

include $(XWMAKE_DIR)/rules/nested.mk


check         : fetch
extract       : check
patch         : extract
link          : patch
pre-configure : link
    configure : pre-configure
depend        : configure
build         : depend
install       : build
install-root  : install
pack          : install-root
pack-status   : install-root

fetch         :         $(FETCH_STAMP)
check         :         $(CHECK_STAMP)
extract       :       $(EXTRACT_STAMP)
patch         :         $(PATCH_STAMP)
link          :          $(LINK_STAMP)
pre-configure : $(PRE_CONFIGURE_STAMP)
    configure :     $(CONFIGURE_STAMP)
depend        :        $(DEPEND_STAMP)
build         :         $(BUILD_STAMP)
install       :       $(INSTALL_STAMP)
install-root  :  $(INSTALL_ROOT_STAMP)
pack          :          $(PACK_STAMP)
pack-status   :   $(PACK_STATUS_STAMP)


              uninstall: unpack unpack-status uninstall-root
      unlink link-clean: uninstall uninstall-root
    unpatch patch-clean: unlink
unextract extract-clean: unpatch
    unfetch fetch-clean: uncheck
    uncheck check-clean: unextract
                 update: unextract


all: install-root


# directory creating rule
%/.dir:
	$(TRACE_TARGET)
	@#echo -n "$(I)Creating directory $(dir $@) ... "
	@$(MKDIR_P) $(dir $@)
	@$(TOUCH) $@
	@#echo 'done'


$(ST_DIR)/% : $(LOG_DIR)/.dir


include $(XWMAKE_DIR)/rules/alias.mk
include $(XWMAKE_DIR)/rules/lastlink.mk
include $(XWMAKE_DIR)/rules/src-depot-list.mk
include $(XWMAKE_DIR)/rules/urilist.mk
include $(XWMAKE_DIR)/rules/log.mk
include $(XWMAKE_DIR)/rules/save.mk
include $(XWMAKE_DIR)/rules/tar.mk
include $(XWMAKE_DIR)/rules/host.mk

include $(XWMAKE_DIR)/rules/fetch.mk
include $(XWMAKE_DIR)/rules/watch.mk
include $(XWMAKE_DIR)/rules/update.mk
include $(XWMAKE_DIR)/rules/check.mk
include $(XWMAKE_DIR)/rules/extract.mk
include $(XWMAKE_DIR)/rules/patch.mk
include $(XWMAKE_DIR)/rules/link.mk
include $(XWMAKE_DIR)/rules/configure.mk
include $(XWMAKE_DIR)/rules/depend.mk
include $(XWMAKE_DIR)/rules/build.mk
include $(XWMAKE_DIR)/rules/install.mk
include $(XWMAKE_DIR)/rules/root.mk
include $(XWMAKE_DIR)/rules/pack.mk

include $(XWMAKE_DIR)/rules/re.mk

# used in both of inter-platform.mk and inter-package.mk
stfile_target = $(patsubst .%,%,$(suffix $@))

include $(XWMAKE_DIR)/rules/inter-platform.mk
include $(XWMAKE_DIR)/rules/inter-package.mk

include $(XWMAKE_DIR)/rules/xdep.mk

-include  $(TOOLS_DIR)/rules.mk
-include $(COMMON_DIR)/rules.mk


endif # xwmake/rules.mk
