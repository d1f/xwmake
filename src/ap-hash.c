#include <string.h>

// (C) Arash Partow http://www.partow.net/programming/hashfunctions/
unsigned int APHash(const char* str, unsigned int len)
{
   unsigned int hash = 0xAAAAAAAA;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash ^= ((i & 1) == 0) ? (  (hash <<  7) ^  (*str) * (hash >> 3)) :
                               (~((hash << 11) + ((*str) ^ (hash >> 5))));
   }

   return hash;
}

unsigned int ap_hash(const char *str)
{
    return APHash(str, strlen(str));
}


#include <stdio.h>
#include <stdlib.h>

int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "Usage: %s string\n", av[0]);
	return EXIT_FAILURE;
    }

    printf("%08x\n", ap_hash(av[1]));
    return EXIT_SUCCESS;
}
