package FileList;

require 5.005;
use strict;
local $^W=1; # use warnings only since 5.006

use File::Spec::Functions qw(splitpath catfile);

sub BEGIN
{
    use Exporter;
    use vars qw(@ISA @EXPORT);

    @ISA = ('Exporter');
    @EXPORT = qw( &basename );
}


sub basename($)
{
   return (splitpath( shift )) [2];
}


sub new($)
{
    my $class = shift; $class = ref $class if ref $class;

    my $this =
    {
	'files' => {},		# key: file name; value: index in @paths
	'paths' => [],		# path list
	'path_idx' => {}	# key: path; value: index of dir name
				#	(temporary path presence marks)
    };

    return bless $this, $class;
}


# accepts: object ref, full file name
# returns: nothing
sub add_file($$)
{
    my $this = shift;
    $_ = shift;

    my $files    = $this->{'files'};	#  hash ref
    my $paths    = $this->{'paths'};	# array ref
    my $path_idx = $this->{'path_idx'};	#  hash ref

    my (undef,$dir,$file) = splitpath( $_ );

    if ( ! exists $path_idx->{$dir} )
    {
	push @{$paths}, $dir;
	$path_idx->{$dir} = scalar(@{$paths}) - 1; # index of the dir name
    }

    if ( ! exists $files->{$file} )
    {
	$files->{$file} = $path_idx->{$dir}; # index of the dir name
    }
}


# accepts: object ref, list file name
# returns: nothing
sub read_files_list($$)
{
    my $this  = shift;
    my $flist = shift;

    local *IN;
    open  (IN, "< $flist")
	or die "Can't open $flist: $!\n";

    while ( <IN> )
    {
	chomp $_;
	$this->add_file( $_ );
    }

    close IN;
}


# accepts: object ref, file name
# returns: path or undef
sub find_file($$)
{
    my $this = shift;
    my $file = shift;

    if ( exists $this->{'files'}->{$file} )
    {
	return $this->{'paths'}->[ $this->{'files'}->{$file} ];
    }
    else
    {
	return undef;
    }
}


# accepts: object ref, file name, suffices list
# returns: full file name or undef
sub find_file_sfx($$@)
{
    my $this = shift;
    my $file = shift;

    foreach (@_)
    {
	my $path = $this->find_file( $file . $_ );
	if ( defined $path )
	{
	    return catfile( $path, ($file . $_) );
	}
    }

    return undef;
}


1;