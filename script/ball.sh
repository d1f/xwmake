#!/bin/sh

# Build all of toolchain variants.
# This script is not intended for ordinary use, only for toolchain testing.

if test "$#" -lt 3; then
   echo 1>&2 "Usage: `basename $0` project target package ..."
   exit 1
fi

   PRJ=$1
TARGET=$2
shift 2

if true; then
bcc_list="gcc"
else
bcc_list="${bcc_list} gcc-2.95"
bcc_list="${bcc_list} gcc-3.3"
bcc_list="${bcc_list} gcc-3.4"
bcc_list="${bcc_list} gcc34"
bcc_list="${bcc_list} gcc-3.4.6" # gentoo
bcc_list="${bcc_list} gcc-4.1"
bcc_list="${bcc_list} gcc-4.1.2" # gentoo
bcc_list="${bcc_list} gcc-4.2"
bcc_list="${bcc_list} gcc-4.3"
bcc_list="${bcc_list} gcc-4.3.1" # gentoo
bcc_list="${bcc_list} gcc-4.3.2" # gentoo
bcc_list="${bcc_list} gcc-4.3.3" # gentoo
fi

platform_mk_list=`echo $PRJ/platform/*.mk` || exit 1
for p in $platform_mk_list; do
    platform=`basename $p .mk` || exit 1
    platform_list="${platform_list} ${platform}"
done

but_list="2.15"
#but_list="${but_list} 2.15.90.0.3"	# crosstool-0.43 patches failed
#but_list="${but_list} 2.15.91.0.2"	# build failed with BUILD_CC=gcc-4.1
					#	due to mess in gas headers
but_list="${but_list} 2.16.1"

gcc_list="2.95.3"
gcc_list="${gcc_list} 3.3.6"
gcc_list="${gcc_list} 3.4.5"
gcc_list="${gcc_list} 4.1.1"

glibc_list="2.2.5"
glibc_list="${glibc_list} 2.3.6"
glibc_list="${glibc_list} 2.4"
glibc_list="${glibc_list} 2.5"

lk_list="2.4.31"
lk_list="${lk_list} 2.4.32"
lk_list="${lk_list} 2.4.34.5"
lk_list="${lk_list} 2.6.25"


case $TARGET in
    *fetch | check | *extract | *patch)
	bcc_list="gcc";;
esac

case $TARGET in
    *fetch | check | *extract)
	platform_list="pcengines_wrap";;
esac


for bcc in $bcc_list; do \
    if test -z "`which $bcc 2>/dev/null`"; then continue; fi
    export BUILD_CC=$bcc
    bcc_ver=`$bcc -dumpversion` || exit 1

    for platform in $platform_list; do \
	export PLATFORM=$platform
	for but in $but_list; do \
	    export BINUTILS_VERSION=$but
	    for gcc in $gcc_list; do \
		export GCC_VERSION=$gcc
		for glibc in $glibc_list; do \
		    GLIBC_VERSION=$glibc

		    case $GLIBC_VERSION in
			# glibc 2.3.6 requires gcc 3.2+
			2.[3-9].*)
			    case $GCC_VERSION in
				2.9*) continue;;
			    esac
			    case $bcc_ver in
				2.9*) continue;;
			    esac
			;;

			# glibc-2.2.5 build failed with gcc 3.4.5
			2.2.*)
			    case $GCC_VERSION in
				3.4*) continue;;
			    esac
			;;
		    esac

		    for lk in $lk_list; do \
			export LK_VERSION=$lk

			case $LK_VERSION in
			    # gcc-4.x is not supported in linux-2.4.x
			    2.4.*)
				case $GCC_VERSION in
				    4.*) continue;;
				esac
			    ;;

			    # glibc 2.2.* does not builds with linux-2.6.x
			    2.6.*)
				case $GLIBC_VERSION in
				    2.2.*) continue;;
				esac
			    ;;
			esac

			echo "==============================================================================="
			echo "BUILD_CC=$bcc PLATFORM=$platform BINUTILS_VERSION=$but GCC_VERSION=$gcc LIBC_VERSION=$glibc LK_VERSION=$lk"
			echo ""
			./b-$PRJ $TARGET "$@" || exit 1
		    done
		done
	    done
	done
    done
done
