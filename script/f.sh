#!/bin/sh

# utility functions

if test "`basename $0`" = "f.sh"; then
   echo "f.sh: This script must be sourced, not executed." 1>&2
   exit 1
fi


# convention: functions return text result in global var rv

if test -z "${_dirname_}"; then
              _dirname_=`dirname "$0"`	|| exit 1
fi

log()
{
    echo "$*" 1>&2
}

logn()
{
    echo -n "$*" 1>&2
}

die()
{
    test -n "$*" && log "$*"
    kill $$
    exit 1
}

absdir()
{	# $1 - directory
	rv=`cd "$1" && /bin/pwd` || die
}

absfile()
{	# $1 - file path
	local _dn=` dirname "$1"` || die
	local _bn=`basename "$1"` || die
	absdir "$_dn"
	rv="$rv"/"$_bn"
}

if test -e $_dirname_/../indent-spaces; then
   . $_dirname_/../indent-spaces
else
   . $_dirname_/indent-spaces
fi

i2space()
{
    echo -n "$1" | sed "s/i/${INDENT_SPACES}/g"
}

_cleanfiles_=
add_clean_files()
{
	_cleanfiles_="$_cleanfiles_ $@"
}

reset_clean_files()
{
	_cleanfiles_=
}

clean_on_exit()
{
   if test -n "$_cleanfiles_"; then
	log ">>> removing $_cleanfiles_"
	rm -rf $_cleanfiles_
   fi
}

set_clean_trap()
{
	# dashism: quote function
	trap "clean_on_exit" EXIT HUP INT QUIT TERM || die
}


make_temp_dir()
{	# $1 - template name
	rv=`mktemp -d /var/tmp/"$1".XXXXXX` || die
	add_clean_files $rv
	log ">>> $rv created"
}


lk_version()
{	# $1 - lksrcdir
	rv=`$_dirname_/lk-version $lksrcdir` || die
}


host_arch()
{
	rv=`uname -m` || die

	case $rv in
	    i?86         ) rv=i386    ;;
	    sun4u        ) rv=sparc64 ;;
	    arm* | sa110 ) rv=arm     ;;
	esac
}


# returns true if ARCH= found
arch_from_mkvars()
{	# $@ - mkvar=val ...
	for i in $@; do
	    case $i in
		ARCH=*)
		    rv=`echo "$i" | cut -d= -f2` || die
		    return 0
		;;
	    esac
	done

	return 1
}


zimage_target()
{	# $1 - arch
	case $1 in
	    alpha) rv=boot   ;;
	      arm) rv=zImage ;;
	     cris) rv=zImage ;;
	     i386) rv=bzImage;;
	     ia64) rv=compressed;;
	     m68k) rv=bzImage   ;;
	    mips|mips64) rv=vmlinux.ecoff;;
	    #.......
	        *) rv=zImage ;;
	esac
}

zimage_file()
{	# $1 - arch
	case $1 in
	    alpha) rv=boot/vmlinux.gz         ;;
	      arm) rv=boot/zImage             ;;
	     cris) rv=boot/compressed/vmlinuz ;;
	     i386) rv=boot/bzImage            ;;
	     ia64) rv=vmlinux.gz              ;;
	     m68k) rv=vmlinux.bz2             ;;
	    mips|mips64) rv=vmlinux.ecoff     ;;
	    #.......
	        *) rv=boot/zImage             ;;
	esac
}

# This function is used to properly escape shell commands, so
# we don't interpret the install command twice. This is useful
# for making commands like 'make CC="gcc -some -options" install'
# work as expected.
shell_escape()
{
	for str in "$@" ; do
		echo -n "\"$str\" "
	done;
	echo
}
