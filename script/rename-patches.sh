#!/bin/sh

# file with lines:
#	prefix file-name
# moves to:
#	prefix-file-name

while read num name; do
	git mv $name "${num}-${name}"
done
