#!/usr/bin/perl -w

require 5.005;
use strict;

my $pn = "chmodall";

sub usage {
	warn "\n".join(" ", @_)."\n" if @_;
	warn <<EOF;

Usage:
	$pn mode type base-dir
		mode: only 'a-w' | 'u+w'
		type: only  'f ' |  'd'

EOF
	exit(1);
}

@ARGV == 3 or usage("invalid number of parameters");

my $mode = $ARGV[0];
my $type = $ARGV[1];
my $dir  = $ARGV[2];

$type =~ m/^f|d$/          or die "$pn: type $type unknown\n";

my ( $setmask, $clrmask);
if ( $mode =~ m/^(a-w)$/ )
{
    $setmask = 00000;
    $clrmask = 00222;
}
elsif ( $mode =~ m/^(u\+w)$/ )
{
    $setmask = 00200;
    $clrmask = 00000;
}
else
{
    die "$pn: mode $mode yet not supported\n";
}

exit 0 unless -e $dir;
die "$pn: $dir is not a directory\n"                 unless -d $dir;
die "$pn: directory $dir is not readable for me\n"   unless -r $dir;
die "$pn: directory $dir is not executable for me\n" unless -x $dir;

open(IN, "LANG=C find $dir -type $type |")
    or die "\n$pn: Can't fork find: $!\n";

$/ = "\n"; # ensure line mode

while ( <IN> )
{
    chomp;
    my $fmode = (stat($_))[2];
    $fmode &= 07777;
    $fmode |=  $setmask;
    $fmode &= ~$clrmask;

    chmod $fmode, $_;
}

close IN;

exit 0;
