#!/bin/sh

vars="PLATFORM"
vars="$vars BUILD_CC  BUILD_CPP  BUILD_CXX"
vars="$vars BUILD_CC_ BUILD_CPP_ BUILD_CXX_"
vars="$vars BINUTILS_VERSION GCC_VERSION LK_VERSION"
vars="$vars LIBC_TYPE LIBC_VERSION"
vars="$vars LABEL"
vars="$vars TC_OLEVEL TC_OFLAG TC_CFLAGS"
vars="$vars MK_JOBS NICE IWTYPE"
vars="$vars CCACHE"

unset     $vars
