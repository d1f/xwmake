ifndef	xwmake/defs.mk
	xwmake/defs.mk = included

ifneq (,)
This makefile requires GNU Make.
endif


include $(XWMAKE_DIR)/defs/util.mk


# .. lang=ru
# �������, ����������� � ������ ������� ������ ����.
# �������� �� ���������: �����
TRACE_TARGET ?=
#TRACE_TARGET  = @echo "$(I)>>> target: $@"

# settings $(MAKEFLAGS)R brokes something with make 4.0
ifeq ($(filter 4.%,$(MAKE_VERSION)),)
# Don't define any builtin rules and variables.
MAKEFLAGS := $(MAKEFLAGS)R
endif

include $(XWMAKE_DIR)/defs/prog.mk

# Delete default suffixes
.SUFFIXES:

# Delete default rules
.DEFAULT:

.DEFAULT:
	$(error $(I)no rules for target "$@")

# Tell GNU make 3.79 not to run in parallel
.NOTPARALLEL:


.PHONY: all none
# first default targets
none:
all:


# package name
#   PKG (supplied from script)
# .. lang=ru
# ��� ������.
# ������������ ������������� ��� �������� ������� :ref:`b-������ <b-project>`
PKG ?= $(error PKG undefined)

include $(XWMAKE_DIR)/defs/dir.mk
include $(XWMAKE_DIR)/defs/dir-defs.mk


# .. lang=ru
# ���� ������ ������� - ������ ���������, ���������� ����� �������.
PKG_SEARCH_PATH  = $(addsuffix /pkg,$(ALL_PRJ_DIRS))


include $(XWMAKE_DIR)/defs/target.mk
include $(XWMAKE_DIR)/defs/func.mk
include $(XWMAKE_DIR)/defs/stamp.mk

-include $(PRJ_DIR)/defs.mk

include $(XWMAKE_DIR)/defs/pkg.mk
include $(XWMAKE_DIR)/defs/patch.mk
include $(XWMAKE_DIR)/defs/log.mk
include $(XWMAKE_DIR)/defs/iw.mk
include $(XWMAKE_DIR)/defs/cmd.mk
include $(XWMAKE_DIR)/defs/watch.mk
include $(XWMAKE_DIR)/defs/xdep.mk


# .. lang=ru
# ������ ���� �������� �������, ������� ��������� "build".
# ������������ �� ������� ������ ����������� ��������.
ALL_PLATFORMS := $(notdir $(basename $(wildcard $(PRJ_DIR)/platform/*.mk)))
ALL_PLATFORMS += build


# .. lang=ru
# ������ �ͣ� ������� �������.
# ������������� �� ������� ������ ������� � ��������� ������������
# ���� ������ �������.
    PRJ_PKGES := $(call pkges_f,$(PRJ_PKG_DIR))

# .. lang=ru
# ������ �ͣ� ������� �������������� ����� ����� ���� ��������.
# ������������ �� ������� ������ ������� � :make:var:`COMMON_PKG_DIR`.
 COMMON_PKGES := $(call pkges_f,$(COMMON_PKG_DIR))

# .. lang=ru
# ������ �ͣ� ������� � tools.
# ������������ �� ������� ������ ������� � :make:var:`TOOLS_PKG_DIR`.
TOOLS_PKGES := $(call pkges_f,$(TOOLS_PKG_DIR))

# .. lang=ru
# ������ �ͣ� ������� ��������� ������� ������.
# ������������ �� ������� ������ ������� � :make:var:`XWMAKE_PKG_DIR`.
 XWMAKE_PKGES := $(call pkges_f,$(XWMAKE_PKG_DIR))

# sort to remove duplicates
# .. lang=ru
# ������ �ͣ� ���� ������� �������, ����� ����� � ���������.
    ALL_PKGES := $(sort $(foreach pkgdir,$(PKG_SEARCH_PATH),\
                        $(call pkges_f,$(pkgdir)) ))

-include $(_ST_DIR)/build-machine.mk
include $(XWMAKE_DIR)/defs/mirrors.mk


TOOLCHAIN_INSTALL_STAMP = $(call tstamp_f,install,toolchain)


endif # xwmake/defs.mk
