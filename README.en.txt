#
# DO NOT DO ANYTHING FROM ROOT !!!
#

Quick start:

Create somewhere in reliable place directory for a source archives:

	mkdir -p <RELIABLE-PLACE>/source-depot

Initial setup:

	./xwmake/setup <RELIABLE-PLACE>/source-depot

Build target images:
	./b install goal

Save all of downloaded source archives in reliable place:
	./b save-all-files

Look at target images:
	ls -lh var/inst/last/images
