# platform (board) constants
# special platform definition for build tools

#ifndef	xwmake/platform/build.mk
	xwmake/platform/build.mk = included


ifneq ($(strip $(PLATFORM)),build)
$(error $(I)wrong target platform name: $(PLATFORM))
endif


TARGET =  $(BUILD)
 ARCH  =  $(BUILD_ARCH)
NARCH  = $(NBUILD_ARCH)

CC=$(BUILD_CC)
CROSS_PREFIX=


#endif # xwmake/platform/build.mk
