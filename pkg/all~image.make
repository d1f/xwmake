# all packages w/o root/boot images

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


filter_out   = all% %all %image %image-ixp firmware checkinstall installwatch
NESTED_PKGES = $(filter-out $(filter_out),$(ALL_PKGES))


include $(RULES)
