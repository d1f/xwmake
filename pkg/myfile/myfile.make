PKG_ORIG_DIR     = $(PKG_PKG_DIR)
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM = build

include $(DEFS)


define BUILD_CMD
  gcc -Wall -W -O3 $(PKG_PKG_DIR)/$(PKG_NAME).c -o $(PKG_BLD_DIR)/$(PKG_NAME) $(LOG)
endef


define INSTALL_CMD
  $(IW) cp -av $(PKG_BLD_DIR)/$(PKG_NAME) $(IBIN_DIR) $(LOG)
endef

    CLEAN_CMD = $(RM) $(PKG_BLD_DIR)/$(PKG_NAME) $(LOG)
DISTCLEAN_CMD = $(CLEAN_CMD)


include $(RULES)
