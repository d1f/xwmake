#include <stdio.h>    // printf
#include <stdlib.h>   // EXIT_*
#include <error.h>    // error
#include <errno.h>    // errno
#include <fcntl.h>    // open
#include <unistd.h>   // close
#include <sys/mman.h> // mmap
#include <string.h>   // memcmp

enum { MAX_SIGNATURE_LENGTH = 8 };

typedef struct
{
    size_t len;
    size_t off;
    const char sig[MAX_SIGNATURE_LENGTH];
    const char *name;
} signature_t;

// https://en.wikipedia.org/wiki/List_of_file_signatures
// this table must be ended with len == 0 and be sorted by maximum len first
static const signature_t signatures[] =
{
    { .len = 8, .sig = { 0x75, 0x73, 0x74, 0x61, 0x72, 0x00, 0x30, 0x30 }, .name="tar", .off = 0x101 }, // ustar
    { .len = 8, .sig = { 0x75, 0x73, 0x74, 0x61, 0x72, 0x20, 0x20, 0x00 }, .name="tar", .off = 0x101 }, // ustar
    { .len = 8, .sig = { 0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x01, 0x00 }, .name="rar"      },
    { .len = 7, .sig = { 0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x00       }, .name="rar"      },
    { .len = 6, .sig = { 0x37, 0x7A, 0xBC, 0xAF, 0x27, 0x1C             }, .name="7zip"     },
    { .len = 5, .sig = { 0xfd, 0x37, 0x7a, 0x58, 0x5a                   }, .name="xz"       }, // .7zXZ
    { .len = 4, .sig = { 0x4C, 0x5A, 0x49, 0x50                         }, .name="lzip"     },
    { .len = 4, .sig = { 0x50, 0x4B, 0x03, 0x04                         }, .name="zip"      },
    { .len = 4, .sig = { 0x50, 0x4B, 0x05, 0x06                         }, .name="zip"      },
    { .len = 4, .sig = { 0x50, 0x4B, 0x07, 0x08                         }, .name="zip"      },
    { .len = 3, .sig = { 0x42, 0x5A, 0x68                               }, .name="bzip2"    },
    { .len = 2, .sig = { 0x1F, 0x8B                                     }, .name="gzip"     },
    { .len = 2, .sig = { 0x1F, 0x9D                                     }, .name="compress" }, // .z
    { .len = 2, .sig = { 0x1F, 0xA0                                     }, .name="compress" }, // .z
    { .len = 0 }
};

int main(int ac, char *av[])
{
    if (ac < 2)
	error(EXIT_FAILURE, 0, "No file name given");
    if (ac > 2)
	error(EXIT_FAILURE, 0, "Extra parameters given");

    int fd = open(av[1], O_RDONLY);
    if (fd < 0)
	error(EXIT_FAILURE, errno, "Can't open file %s", av[1]);

    char *addr = mmap(NULL, MAX_SIGNATURE_LENGTH + 0x200 /*FIXME*/, PROT_READ, MAP_PRIVATE, fd, 0);
    if (addr == MAP_FAILED)
	error(EXIT_FAILURE, errno, "No file name given");

    int ret = EXIT_FAILURE;

    size_t i = 0;
    for (i = 0; signatures[i].len != 0; ++i)
    {
	const signature_t *sig = &signatures[i];
	int rc = memcmp((const char*)addr + sig->off, &sig->sig, sig->len);
	if (rc == 0)
	{
	    printf("%s\n", sig->name);
	    ret = EXIT_SUCCESS;
	    goto out;
	}
    }

    printf("Unknown format\n");

out:
    munmap(addr, MAX_SIGNATURE_LENGTH);
    close(fd);
    return ret;
}
