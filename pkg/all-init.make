# all *init packages

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


NESTED_PKGES = $(filter %init init%,$(filter-out all% %all,$(ALL_PKGES)))


include $(RULES)
